package grammar.util;

import java.util.Iterator;

/**
 * This class implements an iterator with a lookahead (one item) capability
 * @param <T> the types of the items iterated upon
 */
public class LookaheadIterator<T> implements Iterator<T> {
	/** the lookahead token */
	private Optional<T> lookahead;
	
	/** This is the decorator on top of which we add the lookahead decoration */
	private Iterator<? extends T> decoree;
	
	/**
	 * Creates a new instance decorating the 'decoree' iterator given as an argument
	 * @param decoree the decorated object
	 */
	public LookaheadIterator(final Iterator<? extends T> decoree){
		if(decoree == null){ throw new IllegalArgumentException("decoree must be set"); }
		this.decoree = decoree;
		// Take the initial step
		this.lookahead = maybeNext();
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean hasNext() {
		return lookahead.hasValue();
	}

	/** {@inheritDoc} */
	@Override
	public T next() {
		T ret = lookahead.value();
		lookahead = maybeNext();
		return ret;
	}
	/**
	 * @return an optional value representing the next item to be read
	 */
	public Optional<T> peek(){
		return lookahead;
	}
	
	/**
	 * @return an optional object encapsulating the next value (if one!)
	 */
	private Optional<T> maybeNext(){
		return decoree.hasNext() ? OptionalUtil.<T>some(decoree.next()) : OptionalUtil.<T>none();
	}
	
	/** {@inheritDoc} */
	public String toString(){
		return String.format("Lookahead(%s)", lookahead);
	} 
}
