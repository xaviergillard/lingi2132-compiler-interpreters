package grammar.util;

import java.util.NoSuchElementException;

/** 
 * This interface defines an optional object that may or may not have a value. 
 * This is useful since we want to be able to distinguish the case where null must be considered
 * a value.
 */
public interface Optional<T> {
	/**
	 * @return the value encapsulated in this optional object.
	 * @throws NoSuchElementException in case no value is encapsulated
	 */
	T value();
	/**
	 * @return true when a value is encapsulated by this object, false otherwise
	 */
	boolean hasValue();
}
