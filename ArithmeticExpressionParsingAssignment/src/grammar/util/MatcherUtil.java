package grammar.util;

import grammar.util.matches.AnyOfMatcher;
import grammar.util.matches.ExactMatcher;
import grammar.util.matches.NumberMatcher;


/** This class provides the general commodities to create simple matchers */
public final class MatcherUtil {
	/** no public constructor */
	private MatcherUtil(){}
	
	/**
	 * @param s the string that must be matched
	 * @return a matcher matching on that exact string
	 */
	public static Matcher exactly(String s){
		return new ExactMatcher(s);
	}
	/**
	 * @return a matcher matching numbers string (called 'id' in the grammar)
	 */
	public static Matcher number(){
		return NumberMatcher.INSTANCE;
	}
	/**
	 * @param matchers the matchers that can possibly be accepted
	 * @return a matcher matching any of the given matchers
	 */
	public static Matcher anyOf(Matcher...matchers){
		return new AnyOfMatcher(matchers);
	}
	/**
	 * @param tokens the tokens that can be possible matches
	 * @return a matcher matching on any of the given tokens with an exact match
	 */
	public static Matcher anyOf(String...tokens){
		Matcher[] matchers = new Matcher[tokens.length];
		for(int i = 0; i<tokens.length; i++){
			matchers[i] = exactly(tokens[i]);
		}
		return anyOf(matchers);
	}
}
