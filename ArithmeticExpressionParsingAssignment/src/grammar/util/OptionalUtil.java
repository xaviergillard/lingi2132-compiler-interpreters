package grammar.util;

import java.util.NoSuchElementException;

/** This utility class provides the commodities we use to manipulate optional objects */
public final class OptionalUtil {
	/** utility class has no public constructor */
	private OptionalUtil(){}
	
	/** There's no point in having multiple none instances */
	@SuppressWarnings("rawtypes")
	private static final None NONE = None.INSTANCE;

	/** 
	 * This wraps the value as an optional object
	 * @param  val the value to be encapsulated 
	 * @return the given value wrapped as an optional object
	 */
	public static <T> Optional<T> some(T val){
		return new Some<T>(val);
	}
	/**
	 * @return an instance of an optional object telling there's no value
	 */
	@SuppressWarnings("unchecked")
	public static <T> Optional<T> none(){
		return NONE;
	}
	
	/** This class encapsulates the case where an option HAS a value */
	private static class Some<T> implements Optional<T> {
		/** the encapsulated value */
		private final T value;
		public Some(T value){
			this.value = value;
		}
		/** {@inheritDoc} */
		@Override
		public T value(){
			return value;
		}
		/** {@inheritDoc} */
		@Override
		public boolean hasValue(){
			return true;
		}
		
		/** {@inheritDoc} */
		public String toString(){
			return String.format("Some(%s)", value);
		}
	}
	/** This class encapsulates the case where an option HAS NO value */
	private static class None<T> implements Optional<T> {
		/** the singleton instance */
		@SuppressWarnings("rawtypes")
		private static final None INSTANCE = new None();
		
		/** None must be a singleton */
		private None(){}
		
		/** {@inheritDoc} */
		@Override
		public T value(){
			throw new NoSuchElementException();
		}
		/** {@inheritDoc} */
		@Override
		public boolean hasValue(){
			return false;
		}
		/** {@inheritDoc} */
		public String toString(){
			return "None";
		}
	}
}
