package grammar.util.matches;

import grammar.util.Matcher;

import java.util.Arrays;
import java.util.List;

/**
 * This is the matcher to use when you want to match against many different matches
 */
public class AnyOfMatcher implements Matcher {
	/** the many matchers to match against */
	private final List<Matcher> matchers;
	
	/**
	 * creates a new instance
	 * @param matchers the matchers to match against
	 */
	public AnyOfMatcher(Matcher...matchers){
		this.matchers = Arrays.asList(matchers);
	}
	
	/** {@inheritDoc} */
	public boolean matches(String token){
		for(Matcher m : matchers){
			if(m.matches(token)){
				return true;
			}
		}
		return false;
	}
	/** {@inheritDoc} */
	public String toString(){
		StringBuilder builder = new StringBuilder();
		for(Matcher m:matchers){ builder.append(m); }
		return String.format("anyOf(%s)", builder.toString());
	}
}
