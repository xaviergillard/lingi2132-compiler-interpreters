package grammar.util.matches;

import grammar.util.Matcher;

/**
 * This is the matcher that implements the exact match, that is to say
 * it is ie the one to use to recognize keywords etc..
 */
public class ExactMatcher implements Matcher {
	/** The token we want to discover */
	private final String expected;
	
	/** 
	 * Creates a new instance
	 * @param expected the expected token
	 */
	public ExactMatcher(String expected){
		this.expected = expected;
	}
	
	/** {@inheritDoc} */
	@Override
	public boolean matches(String token) {
		if(token==null){
			return false;
		}
		return token.equals(expected);
	}
	/** {@inheritDoc} */
	public String toString(){
		return String.format("exactly(%s)", expected);
	}
}
