package grammar.util.matches;

import grammar.util.Matcher;

/**
 * This is the strategy to recognize some number string
 */
public class NumberMatcher implements Matcher {
	/** this is the singleton instance */
	public static final Matcher INSTANCE = new NumberMatcher();
	
	/** This is the pattern used to recognize an identifier */
	private static final String PATTERN = "0|([1-9]\\d*)";
	
	/** This is a singleton: it has no public constructor */
	private NumberMatcher() {}
	
	/** {@inheritDoc} */
	@Override
	public boolean matches(String token) {
		if(token==null){
			return false;
		}
		return token.matches(PATTERN);
	}
	/** {@inheritDoc} */
	public String toString(){
		return "0 | (1-9){0-9}";
	}
}
