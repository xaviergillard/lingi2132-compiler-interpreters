package grammar.util;

/**
 * Interface defined to easily determine whether a token matches
 * some given pattern
 */
public interface Matcher {
	/**
	 * Returns true if the token is considered to be a match
	 * @param token the token to match this strategy against
	 * @return true if the token is considered to be a match
	 */
	public boolean matches(String token);
}
