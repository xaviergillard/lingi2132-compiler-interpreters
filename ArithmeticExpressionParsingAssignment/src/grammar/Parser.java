package grammar;

import static grammar.Grammar.DIVIDE;
import static grammar.Grammar.LEFTPAR;
import static grammar.Grammar.MINUS;
import static grammar.Grammar.PLUS;
import static grammar.Grammar.RIGHTPAR;
import static grammar.Grammar.TIMES;
import static grammar.util.MatcherUtil.anyOf;
import static grammar.util.MatcherUtil.exactly;
import static grammar.util.MatcherUtil.number;
import grammar.util.LookaheadIterator;
import grammar.util.Matcher;

import java.util.Arrays;

/**
 * This parser parses the following grammar:
 *  E -> E + T
 *	E -> E - T
 *	E -> T 
 *	T -> T * F
 *	T -> T / F
 *	T -> F
 *	F -> ( E )
 *	F -> id
 *	id -> "0" | ("1"-"9") {"0"-"9"} 
 *
 * ADAPTATIONS: 
 * E -> T E'
 * E'-> (+|-) T E' | epsylon
 * T -> F T'
 * T'-> (*|/) F T' | epsylon
 * F -> ( E ) | id
 * 
 * COMME CETTE GRAMMAIRE EST TAIL RECURSIVE, ON PEUT L'IMPLEMENTER AVEC DES BOUCLES
 * (HISTOIRE DE PRESERVER UN PEU LA STACK). DU COUP, CA DONNERAIT:
 * 
 * E -> T E'
 * E'-> {(+|-) T}
 * T -> F T'
 * T'-> {(*|/) F}
 * F -> id | (E)
 * 
 * CE QU'ON PEUT MEME SIMPLIFIER COMME SUIT:
 * E -> T {(+|-) T}
 * T -> F {(*|/) F}
 * F -> id | (E)
 * 
 * POUR ETRE TOUT A FAIT COMPLET, EN FAIT, IL FAUDRAIT DIRE QUE LA GRAMMAIRE DECIDEE PAR CE PARSER
 * EST LA SUIVANTE:
 * 
 * S -> E #
 * E -> T {(+|-) T}
 * T -> F {(*|/) F}
 * F -> id | (E)
 * 
 */
public class Parser {
	/** 
	 * This is the iterator we use in order to implement the LL(1) grammar parsing using
	 * recursive descent. This lookahead iterator comes in handy when it comes to deciding
	 * what path to take based on the next token of input.
	 */
	private LookaheadIterator<String> iterator;
	
	/**
	 * Returns true iff the whole input array can be parsed as an expression of the 
	 * language depicted above. The 'whole input array' means that there cannot be any leftover
	 * in the buffer after the expression has been parsed.
	 * 
	 * @param input the 'stream' of tokens
	 * @return true iff the whole array of input can be interpreted as an expression, false otherwise
	 */
	public boolean parse(String[] input) {
		iterator = new LookaheadIterator<String>(Arrays.asList(input).iterator());
		return ess();
	}
	
	// S -> E #
	private boolean ess(){
		boolean ret = ee();
		// Everything must have been consumed
		return hasNext() ? false : ret;
	}
	
	// E -> T {(+|-) T}
	private boolean ee(){
		boolean ret = tee();
		while(has(anyOf(PLUS, MINUS))){
			consume(anyOf(PLUS, MINUS));
			ret &= tee();
		}
		return ret;
	}
	
	// T -> F {(*|/) F}
	private boolean tee(){
		boolean ret = eff();
		while(has(anyOf(TIMES, DIVIDE))){
			consume(anyOf(TIMES, DIVIDE));
			ret &= eff();
		}
		return ret;
	}
	// F -> id | (E)
	private boolean eff(){
		// F must be exactly either an id (number) or an E wrapped in par
		if(has(number())){
			consume(number());
			return true;
		}
		if(has(exactly(LEFTPAR))){
			consume(exactly(LEFTPAR));
			if(!(hasNext() && ee())){ 
				return false; 
			}
			if(!(hasNext() && has(exactly(RIGHTPAR)))){
				return false;
			} else {
				consume(exactly(RIGHTPAR));
			}
			return true;
		}
		return false;
	}
	
	
	/**
	 * Returns true if the next token satisfies the given matcher criterion.
	 * This method can ie be used as:
	 * has(anyOf("+", "-");
	 * or
	 * has(exactly("Bonjour");
	 * or
	 * has(number());
	 * 
	 * @param matcher the matcher encapsulating the identification to be performed
	 * @return true if the next token (doesn't move the cursor) matches the given matcher
	 */
	private boolean has(Matcher matcher){
		ensureInitialized();
		return iterator.peek().hasValue() &&
			   matcher.matches(iterator.peek().value());
	}
	/**
	 * This is clearer than a plain call to next because it emphasizes on what we do
	 * @param matcher the matcher
	 */
	private void consume(Matcher matcher){
		ensureInitialized();
		String token = iterator.next();
		if(!matcher.matches(token)){
			throw new RuntimeException(
					String.format("Token %s doesnt match %s", token, matcher));
		}
	}

	/** 
	 * Shortcut to delegate to the iterator's hasNext method
	 */
	private boolean hasNext(){
		ensureInitialized();
		return iterator.hasNext();
	}
	
	/**
	 * Ensures the parser is properly initialized
	 */
	private void ensureInitialized(){
		if(iterator==null){
			throw new IllegalStateException(
					"Token iterator wasn't set. You should initialize it first");
		}
	}
}
