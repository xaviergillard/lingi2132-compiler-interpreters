package grammar.util;

import java.util.Arrays;
import java.util.NoSuchElementException;

import junit.framework.Assert;
import junit.framework.TestCase;

@SuppressWarnings("deprecation")
/** This class validates the behavior of the lookahead iterator */
public class TestLookaheadIterator extends TestCase {
	
	public void testItCannotDecorateNull(){
		boolean caught = false;
		try{
			new LookaheadIterator<>(null);
		} catch (IllegalArgumentException ex) {
			caught = true;
		}
		Assert.assertTrue(caught);
	}
	
	public void testAnEmptyIterableHasNoNext(){
		Assert.assertFalse(iterator().hasNext());
	}
	public void testPeekingAnEmptyIteratorYieldsNone(){
		Assert.assertFalse(iterator().peek().hasValue());
	}
	public void testGettingNextFromAnEmptyIteratorThrowsException(){
		boolean caught = false;
		try{
			iterator().next();
		} catch (NoSuchElementException ex) {
			caught = true;
		}
		Assert.assertTrue(caught);
	}
	
	public void testANonEmptyIteratorHasNext(){
		Assert.assertTrue(iterator("Bonjour").hasNext());
	}
	public void testPeekingANonEmptyIteratorShowsWhatIsToCome(){
		LookaheadIterator<String> iterator = iterator("Bonjour");
		
		Optional<String> peeked = iterator.peek();
		Assert.assertNotNull(peeked);
		Assert.assertTrue(peeked.hasValue());
		
		Assert.assertEquals("Bonjour", peeked.value());
	}
	public void testPeekingANonEmptyIteratorDoesntMoveTheCursor(){
		LookaheadIterator<String> iterator = iterator("Bonjour");
		
		Optional<String> peeked = iterator.peek();
		Assert.assertNotNull(peeked);
		Assert.assertTrue(peeked.hasValue());
		Assert.assertTrue(iterator.hasNext());
		Assert.assertEquals(iterator.next(), peeked.value());
		Assert.assertFalse(iterator.hasNext());
	}
	public void testGettingTheNextItemFromANonEmptyIteratorDoesMoveTheCursor(){
		LookaheadIterator<String> iterator = iterator("Bonjour");
		
		Assert.assertTrue(iterator.hasNext());
		Assert.assertEquals("Bonjour", iterator.next());
		Assert.assertFalse(iterator.hasNext());
	}
	
	public void testThereIsNoOffByOneError(){
		// et c'est vrai :D
		LookaheadIterator<String> iterator = iterator("Je", "vais", "être", "papa", "!!");
		
		int count = 0;
		while(iterator.hasNext()){
			Optional<String> peek = iterator.peek();
			String next = iterator.next();
			Assert.assertEquals(next, peek.value());
			count++;
		}
		Assert.assertEquals(5, count);
	}
	
	@SafeVarargs
	private static <T> LookaheadIterator<T> iterator(T...items){
		return new LookaheadIterator<>(Arrays.asList(items).iterator());
	}
}
