package grammar.util;

import java.util.NoSuchElementException;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This class validates the behavior of the Optional objects
 */
@SuppressWarnings("deprecation")
public class TestOptionalUtil extends TestCase {
	/**
	 * Verifies that none never has a value 
	 */
	public void testNoneHasNoValue(){
		Assert.assertFalse(OptionalUtil.none().hasValue());
	}
	/**
	 * Tests that this framework doesn't makeup values
	 */
	public void testGettingTheValueOutOfNoneThrowsAnException(){
		boolean caught = false;
		try{
			OptionalUtil.none().value();
		} catch (NoSuchElementException nse) {
			caught = true;
		}
		Assert.assertTrue(caught);
	}
	/** 
	 * Some should always have a value
	 */
	public void testSomeHasAValue(){
		Assert.assertTrue(OptionalUtil.some("Bonjour").hasValue());
	}
	/**
	 * Some should alwys have a values 
	 */
	public void testSomeHasAValueEvenIfItsNull(){
		Assert.assertTrue(OptionalUtil.some(null).hasValue());
	}
	/**
	 * Verifies that you can get what you give
	 */
	public void testValueReturnsTheEncapsulatedObject(){
		Assert.assertEquals("Bonjour", OptionalUtil.some("Bonjour").value());
	}
	/**
	 * Verifies that you can get what you give
	 */
	public void testNullIsAnAcceptableValue(){
		Assert.assertNull(OptionalUtil.some(null).value());
	}
}
