package grammar.util.matches;

import junit.framework.TestCase;
import junit.framework.Assert;
import grammar.util.Matcher;

@SuppressWarnings("deprecation")
public class TestAnyOfMatcher extends TestCase {
	
	public void testItIsFalseWhenNoneIsTrue(){
		Matcher tested = new AnyOfMatcher(no(), no(), no());
		Assert.assertFalse(tested.matches("everything"));
	}
	
	public void testItIsTrueWhenAnyMatcherIsTrue(){
		Matcher tested = new AnyOfMatcher(no(), no(), no(), yes(), no());
		Assert.assertTrue(tested.matches("everything"));
	}
	
	
	private static Matcher yes(){
		return new MockMatcher(true);
	}
	private static Matcher no(){
		return new MockMatcher(false);
	}
	
	private static class MockMatcher implements Matcher {
		private final boolean value;
		
		public MockMatcher(boolean value) {
			this.value = value;
		}
		@Override
		public boolean matches(String token) {
			return value;
		}
		
	}
}
