package grammar.util.matches;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This class validates the behavior of the exact matcher matcher
 */
@SuppressWarnings("deprecation")
public class TestExactMatcher extends TestCase {
	
	public void testItIsFalseWhenStringsDiffer(){
		Assert.assertFalse(new ExactMatcher("something").matches("different"));
	}
	public void testItIsTrueWhenStringsAreEqual(){
		Assert.assertTrue(new ExactMatcher("something").matches("something"));
	}
}
