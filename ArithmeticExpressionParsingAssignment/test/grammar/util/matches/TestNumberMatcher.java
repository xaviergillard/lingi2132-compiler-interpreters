package grammar.util.matches;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This class validates the behavior of the exact matcher matcher
 */
@SuppressWarnings("deprecation")
public class TestNumberMatcher extends TestCase {
	
	public void testItDoesntMatchWhenItStartsWithZero(){
		Assert.assertFalse(NumberMatcher.INSTANCE.matches("042"));
	}
	public void testItDoesntMatchWhenThereIsALetter(){
		Assert.assertFalse(NumberMatcher.INSTANCE.matches("42b"));
	}
	public void testItDoesntMatchWhenThereIsANonAlphaCharacter(){
		Assert.assertFalse(NumberMatcher.INSTANCE.matches("4!2"));
	}
	public void testItMatchesWhenZeroIsAlone(){
		Assert.assertTrue(NumberMatcher.INSTANCE.matches("0"));
	}
	public void testItMatchesWhenThereIsOneNumber(){
		Assert.assertTrue(NumberMatcher.INSTANCE.matches("4"));
	}
	public void testItMatchesWhenThereAreManyNumber(){
		Assert.assertTrue(NumberMatcher.INSTANCE.matches("4001"));
	}
}
