package grammar;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This test class verifies the behavior of the Parser
 */
@SuppressWarnings("deprecation")
public class ParserTest extends TestCase {
	/** This is the parser we'll test */
	private Parser parser;
	
	/** This is the generator we'll use to generate the tests */
	private Generator generator;
	
	/** {@inheritDoc} */
	public void setUp(){
		Long seed = System.currentTimeMillis();
		generator = new Generator(seed.intValue());
		parser    = new Parser();
	}
	
	/** {@inheritDoc} */
	public void tearDown(){
		generator = null;
		parser    = null;
	}
	
	public void testEmptyWordIsRejected(){
		Assert.assertFalse(parser.parse(new String[]{}));
	}
	public void testOnlySingleParenthesisIsRejected(){
		Assert.assertFalse(parser.parse(new String[]{"("}));
	}
	public void testOnlyParenthesesIsRejected(){
		Assert.assertFalse(parser.parse(new String[]{"(", ")"}));
	}
	public void testUnexpectedTokenIsRejected(){
		Assert.assertFalse(parser.parse(new String[]{"Bonjwaaar"}));
	}
	public void testSyntaxErrorIsRejected(){
		Assert.assertFalse(parser.parse(new String[]{"1","+","2","-","x"}));
	}
	public void testThereIsNoUnaryOperator(){
		Assert.assertFalse(parser.parse(new String[]{"-","4"}));
	}
	public void testNumberMustStartWithNonZero(){
		Assert.assertFalse(parser.parse(new String[]{"04"}));
		Assert.assertTrue(parser.parse(new String[]{"42"}));
	}
	public void testBracesMustBeMatched(){
		Assert.assertFalse(parser.parse(new String[]{"(","42"}));
		Assert.assertTrue(parser.parse(new String[]{"(","42",")"}));
	}
	public void testParenthesesCanBeNested(){
		Assert.assertTrue(parser.parse(new String[]{"(","(","(","(","(","42",")",")",")",")",")"}));
	}
	public void testBothOperandsMustBePresent(){
		Assert.assertFalse(parser.parse(new String[]{"+"}));
		Assert.assertFalse(parser.parse(new String[]{"-"}));
		Assert.assertFalse(parser.parse(new String[]{"*"}));
		Assert.assertFalse(parser.parse(new String[]{"/"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "+"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "-"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "*"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "/"}));
		Assert.assertFalse(parser.parse(new String[]{"+", "42"}));
		Assert.assertFalse(parser.parse(new String[]{"-", "42"}));
		Assert.assertFalse(parser.parse(new String[]{"*", "42"}));
		Assert.assertFalse(parser.parse(new String[]{"/", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "+", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "-", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "*", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "/", "42"}));
	}
	public void testOperationsCanBeChained(){
		Assert.assertTrue(parser.parse(new String[]{"42", "+", "42", "+", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "-", "42", "-", "42"}));
		
		Assert.assertTrue(parser.parse(new String[]{"42", "*", "42", "*", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "/", "42", "/", "42"}));
	}
	public void testOperationsCanBeMixed(){
		Assert.assertTrue(parser.parse(new String[]{"42", "+", "42", "-", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "+", "42", "*", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "+", "42", "/", "42"}));
		
		Assert.assertTrue(parser.parse(new String[]{"42", "-", "42", "+", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "-", "42", "*", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "-", "42", "/", "42"}));
		
		Assert.assertTrue(parser.parse(new String[]{"42", "*", "42", "+", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "*", "42", "-", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "*", "42", "/", "42"}));
		
		Assert.assertTrue(parser.parse(new String[]{"42", "/", "42", "+", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "/", "42", "-", "42"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "/", "42", "*", "42"}));
	}
	public void testParenthesedOperationIsConsideredLikeANumber(){
		Assert.assertTrue(parser.parse(new String[]{"42", "+", "(", "42", "-", "42", ")"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "-", "(", "42", "-", "42", ")"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "*", "(", "42", "-", "42", ")"}));
		Assert.assertTrue(parser.parse(new String[]{"42", "/", "(", "42", "-", "42", ")"}));
	}
	
	public void testThereAreNeverTwoOperatorsInARow(){
		Assert.assertFalse(parser.parse(new String[]{"42", "+", "+", "(", "42", "-", "42", ")"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "-", "+", "(", "42", "-", "42", ")"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "*", "+", "(", "42", "-", "42", ")"}));
		Assert.assertFalse(parser.parse(new String[]{"42", "/", "+", "(", "42", "-", "42", ")"}));
	}
	
	public void testItMustConsumeEverything(){
		Assert.assertFalse(parser.parse(new String[]{"42", "42"}));
	}
	
	public void testDepth1(){
		checkDepth(1);
	}
	public void testDepth2(){
		checkDepth(2);
	}
	public void testDepth5(){
		checkDepth(5);
	}
	public void testDepth10(){
		checkDepth(10);
	}
	public void testDepth100(){
		checkDepth(100);
	}
	
	private void checkDepth(int n){
		String[] gen = generator.generate(2);
//		echo(gen);
		Assert.assertTrue(parser.parse(gen));
	}
	
//	private void echo(String[] args){
//		StringBuilder sb = new StringBuilder();
//		for(String s : args){
//			sb.append(s);
//		}
//		System.out.println(sb.toString());
//	}
}
