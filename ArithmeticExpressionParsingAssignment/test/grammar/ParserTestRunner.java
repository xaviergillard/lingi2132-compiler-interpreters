package grammar;

import grammar.util.TestLookaheadIterator;
import grammar.util.TestOptionalUtil;
import grammar.util.matches.TestAnyOfMatcher;
import grammar.util.matches.TestExactMatcher;
import grammar.util.matches.TestNumberMatcher;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

/**
 * This is the test suite that gathers all the tests implemented to validate the
 * behavior of the recursive descent parser.
 */
public final class ParserTestRunner extends TestSuite {
	/**
	 * An utility class should have no public constructor
	 */
	private ParserTestRunner(){}
	
	/** 
	 * @return an instance of a test suite properly configured
	 */
	public static TestSuite suite(){
		TestSuite suite = new TestSuite();
		suite.addTestSuite(TestOptionalUtil.class);
		suite.addTestSuite(TestLookaheadIterator.class);
		
		suite.addTestSuite(TestExactMatcher.class);
		suite.addTestSuite(TestNumberMatcher.class);
		suite.addTestSuite(TestAnyOfMatcher.class);
		
		suite.addTestSuite(ParserTest.class);
		return suite;
	}
	
	/**
	 * The program's entry point
	 * @param args the command line arguments
	 */
	public static void main(String[]args){
		TestRunner.run(suite());
	}
}
