//
// This file is the SBT (Scala Build Tool) build file. 
//
// You might consider it a Makefile on steroids: not only does it lets you control the build of your application but
// also the testing, the dependencies to use, how to package the application and so on.
//
// It is also a very convenient way to distribute your program because thanks to the many existing plugins, you don't 
// need to bundle all dependencies with your source code nor is your team bound by a specific IDE. Thanks to this, each
// developer can work with its favourite IDE and keep the project consistent.
// 
// If you are not familiar with SBT, you can either trust us that this file is correct or get it installed and read the
// documentation on http://www.scala-sbt.org.
//
// NOTE: The 'project' folder contains additional information (ie the configuration of the plugins [import in eclipse])
//

//
// If you want to import this project in eclipse, perform the following steps: 
// 1. Get sbt installed
// 2. From the command prompt run 'sbt eclipse'
// 3. Import the project in eclipse
//

lazy val commonSettings = Seq (
  name         := "jungdsl"  ,
  version      := "1.0"         ,
  scalaVersion := "2.11.0"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings{
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")
  }.
  settings{
    libraryDependencies ++= Seq (
      // Jung dependencies, copied over from 'jung2.pom'
      "net.sf.jung"    % "jung-api"           % "2.0.1",
      "net.sf.jung"    % "jung-graph-impl"    % "2.0.1",
      "net.sf.jung"    % "jung-algorithms"    % "2.0.1",
      "net.sf.jung"    % "jung-io"            % "2.0.1",
      "net.sf.jung"    % "jung-visualization" % "2.0.1",
      "net.sf.jung"    % "jung-samples"       % "2.0.1",
      // Beyond this line are nothing but the test libraries
      "org.scalatest"  % "scalatest_2.11"     % "2.2.4"  % "test",
      "org.scalacheck" %%"scalacheck"         % "1.12.2" % "test"
    ) 
  }
