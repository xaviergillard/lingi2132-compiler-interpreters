package example

import jungdsl._
import scala.language.{implicitConversions, postfixOps}

/**
 * This example covers many of the features of the language
 */
object SummarizingExample extends App {
  // We can create graphs with some factory
  val graph = Graph[String, Int]
  // or doit using the raw class
  //val graph = new SparseMultigraph[String, Int]

  // We can add verticess
  graph += "A"
  // We can customize the color (draw, fill), stroke and label of the edges while adding them
  graph += "B" drawIn Blue fillWith Blue withStroke Dotted withLabel "B"
  // We can customize them even after they are added
  graph("A").fill = Yellow            // Using the field notation
  graph("B") withShape Square(20)     // or the keywords

  // We can add unvalued edge
  graph += "A" --- "B"
  graph += "C" --> "D"                // it can add vertices to create an edge

  // We can add valued edges
  graph += "E" -- 3 --> "F"
  graph += "G" -- 4 --> "H"
  // we can have multiple edges for the same endpoints
  graph += "E" -- 5 --- "F"

  // We can customise the color (draw, fill), stroke and label of the edges
  graph += "Biip" -- 6 --> "Muut" drawIn Red fillArrowIn Red drawArrowIn Red withStroke Dotted withLabel "GameBoy"

  // We can display the graph in edition mode for interactive edition (creation)
  // We can specify what layout and dimensions to use
  Display(graph) withDimension (200 x 300) withLayout Circle inEditMode
  // or we can show it in read only mode
  //Display(graph) inReadOnlyMode
}
