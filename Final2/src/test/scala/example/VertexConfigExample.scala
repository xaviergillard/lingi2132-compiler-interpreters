package example

import java.awt.{Color, Font}

import jungdsl._

import scala.language.postfixOps

/**
 * This example is meant to verify the behavior of the vertex configuration
 */
object VertexConfigExample extends App {
  val f = new Font(Font.MONOSPACED, Font.ITALIC, 12)
  val graph = Graph[String, Int]

  graph += "A" drawIn Red fillWith Yellow withStroke Dotted withLabel "A" withFont f withShape Square(30)

  graph("A").shape = Circular(30)

  Display(graph) inReadOnlyMode
}
