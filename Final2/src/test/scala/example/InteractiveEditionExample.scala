package example

import jungdsl._
import scala.language.postfixOps

/**
 * This example shows how to use the interactive edition feature
 */
object InteractiveEditionExample extends App {
  val graph = Graph[Int, Int]

  Display(graph) inEditMode
}
