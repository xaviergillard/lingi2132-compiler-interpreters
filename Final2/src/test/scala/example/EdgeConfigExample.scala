package example

import java.awt.Font

import jungdsl._
import scala.language.postfixOps

/**
 * This example is meant to verify the behavior of the edge configuration
 */
object EdgeConfigExample extends App {
  val f = new Font(Font.MONOSPACED, Font.ITALIC, 12)
  val graph = Graph[String, Int]

  graph += ("A" --> "B" drawIn Cyan fillWith Yellow withStroke Dotted withLabel "A2B" withFont f
                        drawArrowIn Red fillArrowIn Green withArrowStroke Dashed)

  Display(graph) inReadOnlyMode
}
