package example

import jungdsl._
import scala.language.postfixOps

/**
 * This example shows in a very simple way the various possibilities to use the dsl to create a graph
 */
object CreationExample extends App {
  val graph = Graph[String, Int]

  // explicit vertex addition
  graph += "A"

  // unvalued undirected edge
  graph += "B" --- "C"
  // unvalued directed edge
  graph += "D" --> "E"

  // valued undirected edge
  graph += "F" -- 5 --- "G"
  // valued directed edge
  graph += "H" -- 6 --> "I"

  Display(graph) inReadOnlyMode
}
