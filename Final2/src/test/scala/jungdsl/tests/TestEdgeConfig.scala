package jungdsl.tests

import java.awt.{Font, Color}
import javax.swing.ImageIcon

import jungdsl._
import org.scalatest.{Matchers, FlatSpec}

// NOTE: These tests are very weak since they dont effectively test the rendering.
//       At least they verify the syntax and its effects
class TestEdgeConfig  extends FlatSpec with Matchers {
  "The 'draw' of an edge" can "be configured with drawIn" in {
    val e = 1 --> 2 drawIn Color.RED
    e.draw should be (Color.RED)
  }
  "The 'fill' of a vertex" can "be configured with fillWith" in {
    val e = 1 --> 2 fillWith Color.RED
    e.fill should be (Color.RED)
  }
  "The 'stroke' of an edge" can "be configured with withStroke" in {
    val e = 1 --> 2 withStroke Dotted
    e.stroke should be (Dotted)
  }
  "The 'label' of an edge" can "be configured with withLabel" in {
    val e = 1 --> 2 withLabel "OneToTwo"
    e.label should be ("OneToTwo")
  }
  "The 'font' of an edge" can "be configured with withFont" in {
    val f = new Font(Font.MONOSPACED, Font.ITALIC, 12)
    val e = 1 --> 2 withFont f
    e.font should be (f)
  }

  "The 'arrow fill' of an edge" can "be configured with fillArrowIn" in {
    val e = 1 --> 2 fillArrowIn Color.RED
    e.arrow.fill should be (Color.RED)
  }
  "The 'arrow draw' of an edge" can "be configured with drawArrowIn" in {
    val e = 1 --> 2 drawArrowIn Color.RED
    e.arrow.draw should be (Color.RED)
  }
  "The 'arrow stroke' of an edge" can "be configured with withArrowStroke" in {
    val e = 1 --> 2 withArrowStroke Dotted
    e.arrow.stroke should be (Dotted)
  }

  "All config" must "be chainable" in {
    val f = new Font(Font.MONOSPACED, Font.ITALIC, 12)
    val e = 1 --> 2 drawIn Color.RED fillWith Color.RED withStroke Dotted withLabel "OneToTwo" withFont f drawArrowIn Color.RED fillArrowIn Color.RED withArrowStroke Dashed

    e.draw   should equal (Color.RED)
    e.fill   should equal (Color.RED)
    e.stroke should equal (Dotted)
    e.label  should equal ("OneToTwo")
    e.font   should equal (f)
    e.arrow.fill  should equal (Color.RED)
    e.arrow.draw  should equal (Color.RED)
    e.arrow.stroke  should equal (Dashed)
  }
}
