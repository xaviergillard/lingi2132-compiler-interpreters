package jungdsl.tests

import java.awt.{Font, Graphics, Component, Color}
import javax.swing.{ImageIcon, Icon}

import jungdsl._
import jungdsl.graphing.VertexOps
import org.scalatest.{Matchers, FlatSpec}

// NOTE: These tests are very weak since they dont effectively test the rendering
//       At least they verify the syntax and its effects
class TestVertexConfig extends FlatSpec with Matchers {
  "The 'draw' of a vertex" can "be configured with drawIn" in {
    val v = "A" drawIn Color.RED
    v.draw should be (Color.RED)
  }
  "The 'fill' of a vertex" can "be configured with fillWith"in {
    val v = "A" fillWith Color.RED
    v.fill should be (Color.RED)
  }
  "The 'stroke' of a vertex" can "be configured with withStroke"in {
    val v = "A" withStroke Dotted
    v.stroke should be (Dotted)
  }
  "The 'shape' of a vertex" can "be configured with withShape" in {
    val v = "A" withShape Square(20)
    v.shape should equal(Square(20))
  }
  "The 'icon' of a vertex" can "be configured with withIcon" in {
    val i = new ImageIcon()
    val v = "A" withIcon i
    v.icon should be(i)
  }
  "The 'label' of a vertex" can "be configured with withLabel" in {
    val v = "A" withLabel "Goodbye"
    v.label should equal("Goodbye")
  }
  "The 'font' of a vertex" can "be configured with withFont"in {
    val f = new Font(Font.MONOSPACED, Font.ITALIC, 12)
    val v = "A" withFont f
    v.font should be(f)
  }

  "All config" must "be chainable" in {
    val i = new ImageIcon()
    val f = new Font(Font.MONOSPACED, Font.ITALIC, 12)
    val v = 1 drawIn Color.RED fillWith Color.RED withStroke Dotted withLabel "OneToTwo" withFont f withShape Circular(5) withIcon i

    v.draw   should equal (Color.RED)
    v.fill   should equal (Color.RED)
    v.stroke should equal (Dotted)
    v.label  should equal ("OneToTwo")
    v.font   should equal (f)
    v.icon   should equal (i)
    v.shape  should equal (Circular(5))
  }
}
