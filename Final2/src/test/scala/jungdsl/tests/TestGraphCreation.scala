package jungdsl.tests

import jungdsl._
import org.scalatest._
import scala.collection.JavaConversions._
import edu.uci.ics.jung.graph.{DirectedSparseGraph, SparseMultigraph}

class TestGraphCreation extends FlatSpec with Matchers {

  "The dsl" must "permit to add vertices with += " in {
    val graph = Graph[String, Int]
    graph += "A"
    graph.graph.getVertices.toList should equal (List("A"))
    graph.graph.getEdges.toList should equal (List.empty[Int])
  }

  it must "permit to add vertices with += even when the graph is declared with a 'native' constructor" in {
    val graph = new DirectedSparseGraph[String, Int]
    graph += "A"
    graph.graph.getVertices.toList should equal (List("A"))
    graph.graph.getEdges.toList should equal (List.empty[Int])
  }

  it must "permit to add unvalued undirected edges" in {
    val graph = Graph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" --- "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(0))
  }

  it must "permit to add unvalued undirected edges even when the graph is declared with a 'native' constructor" in {
    val graph = new SparseMultigraph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" --- "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(1))
  }

  it must "permit to add unvalued directed edges" in {
    val graph = Graph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" --> "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(2))
  }

  it must "permit to add unvalued directed edges even when the graph is declared with a 'native' constructor" in {
    val graph = new SparseMultigraph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" --> "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(3))
  }

  it must "add the vertices if it wasn't done manually" in {
    val graph = Graph[String, Int]
    graph += "A" --- "B"
    graph += "C" --> "D"
    graph += "E" -- 6 --- "F"
    graph += "G" -- 7 --> "H"
    graph.graph.getVertices.toList should equal (List("A", "B", "C", "D", "E", "F", "G", "H"))
    graph.graph.getEdges.toList should equal (List(4, 5, 6, 7))
  }
  it must "add the vertices if it wasn't done manually even when the graph is declared with a 'native' constructor" in {
    val graph = new SparseMultigraph[String, Int]
    graph += "A" --- "B"
    graph += "C" --> "D"
    graph += "E" -- 8 --- "F"
    graph += "G" -- 9 --> "H"
    graph.graph.getVertices.toList should equal (List("A", "B", "C", "D", "E", "F", "G", "H"))
    graph.graph.getEdges.toList should equal (List(6, 7, 8, 9))
  }

  it must "permit to add valued undirected edges" in {
    val graph = Graph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" -- 1 --- "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(1))
  }

  it must "permit to add valued undirected edges even when the graph is declared with 'native' constructor" in {
    val graph = new SparseMultigraph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" -- 1 --- "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(1))
  }

  it must "permit to add valued directed edges" in {
    val graph = Graph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" -- 1 --> "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(1))
  }
  it must "permit to add valued directed edges even when the graph is declared with 'native' constructor" in {
    val graph = new SparseMultigraph[String, Int]
    graph += "A"
    graph += "B"
    graph += "A" -- 1 --> "B"
    graph.graph.getVertices.toList should equal (List("A", "B"))
    graph.graph.getEdges.toList should equal (List(1))
  }
}
