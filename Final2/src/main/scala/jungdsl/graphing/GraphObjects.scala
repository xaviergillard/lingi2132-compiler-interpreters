package jungdsl.graphing

/**
 * This file contains all the core classes and objects used to manipulate
 * the graphs. As such, it is probably the best entry-point to understand
 * the jungdsl in its entirety.
 */

import scala.collection._
import jungdsl.customization._
import edu.uci.ics.jung.graph.{SparseMultigraph, Graph}
import edu.uci.ics.jung.graph.util.EdgeType
import org.apache.commons.collections15.Factory

/**
 * This class encapsulates the operations that can possibly be performed
 * on an edge (so far only configuration but it could be extended).
 *
 * NOTE: There is currently no body to the class and it is not necessary to
 * have one so far. This is due to the fact that all behavior comes either
 * from the traits or the constructor val-parameters
 *
 * @param value the value associated with the edge itself. Altough this
 *              may seem a little awkward, to have a value associated to
 *              an edge, this is how Jung functions. Therefore, we have
 *              decided not to enforce anything on the existing structure
 *              and keep a value associated with each edge. The main
 *              advantage of this approach is that the enduser can keep
 *              using the existing algorithms on the graphs without noticing
 *              the interference of this DSL.
 * @param kind  this is the type of the edge. It can either be Directed or
 *              undirected.
 * @param v1    this is the value of the 1st endpoint this edge connects to
 * @param v2    this is the value of the 2nd endpoint this edge connects to
 * @tparam V    this is the type of the value held in the endpoints connected
 *              by this edge.
 */
class EdgeOps[V](val value:Any, val kind:EdgeType)(val v1:V, val v2:V)
  extends VisualCustomisation[EdgeOps[V]]
  with LabelCustomisation[EdgeOps[V]]
  with ArrowHeadCustomisation[EdgeOps[V]]
  with SideEffect[EdgeOps[V]]

/**
 * This class represents the source of an edge. Since this may seem a little vage
 * as description, it must be considered as an edge that is CURRENTLY BEING DRAWN.
 * It is therefore not yet complete as, so far, it only has one vertex associated
 * and we don't know yet whether it is going to be a directed or undirected edge.
 * @param  v1 the 1st vertex associated with this edge being drawn.
 * @tparam V  the type of the vertex value
 */
class EdgeSource[V](v1:V) {
  /**
   * This indicates that we want to create an edge with an explicit value. Therefore
   * the -- is meant to indicate that it is only the BEGINNING of the edge. A second
   * half will be needed for that edge to be complete
   * @param e the value associated with the edge being drawn.
   * @return ValuedEdgeSource that correspond to the status of the edge being drawn
   */
  def --[E](e:E) = new ValuedEdgeSource[V,E](v1, e)
  /** This creates a DIRECTED edge which as no explicit value associated */
  def -->(v2:V)  = new EdgeOps[V](null, EdgeType.DIRECTED)(v1, v2)
  /** This creates an UNDIRECTED edge which as no explicit value associated */
  def ---(v2:V)  = new EdgeOps[V](null, EdgeType.UNDIRECTED)(v1, v2)
}

/**
 * This class represents an edge under construction for which we already know the
 * source endpoint and the explicitly associated value.
 *
 * @param v1 this is the source endpoint of the edge
 * @param  e this is the explicit edge value
 * @tparam V this is the type of the vertices
 * @tparam E this is the type of the value associated with the edge.
 */
class ValuedEdgeSource[V,E](val v1:V, val e:E) {
  /** this creates a directed edge binding v1 and v2 with an associated value e */
  def ---(v2:V)  = new EdgeOps[V](e, EdgeType.UNDIRECTED)(v1, v2)
  /** this creates an undirected edge binding v1 and v2 with an associated value e */
  def -->(v2:V)  = new EdgeOps[V](e, EdgeType.DIRECTED)(v1, v2)
}

/**
 * This class encapsulates the operations one can possibly perform on a vertex (so far only
 * customisation).
 *
 * NOTE: This class has no body (and none is required) since all the behavior either comes
 * from the traits or comes from constructor val-parameters.
 *
 * @param value the value associated with this vertex
 * @tparam V the type of the associated value
 */
class VertexOps[V](val value:V)
  extends AdvancedVisualCustomisation[VertexOps[V]]
  with LabelCustomisation[VertexOps[V]]
  with SideEffect[VertexOps[V]]

/**
 * This class encapsulates the various operations the DSL foresees to manipulate the graph
 * (directly). Given the fact that we have decided to not interfere at all with the existing
 * structure of the Jung Graph (such as to allow the end user to e.g. use the Jung algorithms
 * on the graph he/she just created), we have decided to keep in this object a map of all the
 * vertices and edges with their associated VertexOps and EdgeOps.
 *
 * The fact that we need to keep the Vertex- and EdgeOps is mainly due to the fact that in Jung
 * the configuration of the graph (its semantic) is completely decoupled from its presentation.
 * Therefore, this GraphOps tries to bridge the two aspects and memorizes all the configurations
 * that have been performed.
 *
 * @param graph    the real jung graph we are manipulating
 * @param factory  the edge value factory to use when we want to create an edge with no explicit
 *                 value
 * @tparam V the type of the vertices in the graph
 * @tparam E the type of the edges in the graph
 */
class GraphOps[V,E](val graph:Graph[V,E], factory:Factory[E]){
  /** this map keeps track of what vertex is associated with what VertexOps (config) */
  val vertices = mutable.Map.empty[V, VertexOps[V]]
  /** this map keeps track of what edge is associated with what EdgeOps (config) */
  val edges    = mutable.Map.empty[E, EdgeOps[V]]

  /** @return the VertexOps associated with the vertex v (ATTENTION: it must exist) */
  def apply(v:V) = vertices(v)

  /**
   * Adds one vertex to the graph and keeps track of its configuration for the moment
   * when we will want to render it on screen
   *
   * @param v the VertexOps that contains both the value of the object and its config
   */
  def +=(v:VertexOps[V]) {
    vertices += v.value -> v
    graph.addVertex(v.value)
  }

  /**
   * Adds an edge to the graph and keeps track of its configuration for the moment when
   * we will want to render it on screen.
   * @param e the object that contains both the edge value, type and its config
   */
  def +=(e:EdgeOps[V]) {
    /*
     * Important notice:
     * Because it was not possible to force the compiler to infer the right [E] type during
     * the conversion of any to EdgeSource, and because we didn't want to clutter up the
     * notation with something as "Hello" -->[Int] "Goodbye" which kind of defeats the use
     * for a --> way of creating an edge; the --> creation creates a **null value** and this
     * function takes care of calling the necessary factory.create(). Obviously this call to
     * factory.create() was made mandatory by the fact that graphs refuse edges with a null
     * value.
     */
    val eV:E = e.value match {
      case null   => factory.create()
      case other  => other.asInstanceOf[E]
    }
    edges += eV -> e

    /*
     * In case the edge we're adding binds vertices for which we don't know any configuration
     * yet, we need to make and remember a configuration for these vertices.
     */
    if(!vertices.contains(e.v1)) vertices+= e.v1 -> new VertexOps[V](e.v1)
    if(!vertices.contains(e.v2)) vertices+= e.v2 -> new VertexOps[V](e.v2)

    /* finally we add the edge to the graph */
    graph.addEdge(eV, e.v1, e.v2, e.kind)
  }
}

/**
 * This is the companion object of the GraphOps class. Its main purpose is to give the user
 * the ability to create a GraphOps using an implicit factory for the edges. That makes for
 * short compact and user-friendly notation in the user's script.
 */
object GraphOps {
  /**
   * This method creates a GraphOps based on an other type of graph which was specified by
   * the end-user.
   * @param graph the graph to be wrapped in the GraphOps
   * @param factory the factory to use when we need to create a value for an edge that has none.
   * @tparam V the type of the vertices
   * @tparam E the type of the edges
   * @return a GraphOps configured appropriately 'add' some operations on the given 'graph'.
   */
  def apply[V,E](graph:Graph[V,E])(implicit factory:Factory[E]) =
    new GraphOps[V,E](graph, factory)

  /**
   * This method creates a GraphOps and instanciates itself an underlying graph to be manipulated.
   * @param factory the factory to use when we need to create a value for an edge that has none.
   * @tparam V the type of the vertices
   * @tparam E the type of the edges
   * @return a GraphOps whose underlying graph has been instanciated specifically for this purpose
   */
  def apply[V,E](implicit factory:Factory[E]) =
    new GraphOps[V,E](new SparseMultigraph[V,E](), factory)
}