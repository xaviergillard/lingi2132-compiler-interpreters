package jungdsl.graphing

import scala.language.implicitConversions
import org.apache.commons.collections15.Factory

/**
 * This trait provides a convenient way to deal with the edges and vertices factories.
 * For instance it provides implicit factories for all the native types of scala.
 */
trait Factories {
  /**
   * This function permits the implicit conversion of a an expression to a factory
   * that yields its invocation result. It is useful as a way to alleviate the
   * writing.
   * @param  f the expression whose evaluation will be the outcome of the factory.create
   * @tparam O the output type of the expression (that is to say the type of the factory)
   * @return a factory whose create method returns the result of the expression passed by name
   */
  implicit def fn2fact[O](f: => O) = new Factory[O] {def create = f}

  /** This counter is used to implement the different factories */
  private var counter = 0
  /** This method consumes one value in the sequence of the possible counter values */
  private def next    = {val r = counter; counter += 1; r}

  // The name of the different factories should be sufficiently self-explanatory
  implicit val BooleanFactory:Factory[Boolean]= false
  implicit val StringFactory:Factory[String]  = next.toString
  implicit val ByteFactory:Factory[Byte]      = next.toByte
  implicit val CharFactory:Factory[Char]      = next.toChar
  implicit val ShortFactory:Factory[Short]    = next.toShort
  implicit val IntFactory:Factory[Int]        = next.toInt
  implicit val LongFactory:Factory[Long]      = next.toLong
  implicit val FloatFactory:Factory[Float]    = next.toFloat
  implicit val DoubleFactory:Factory[Double]  = next.toDouble
}
