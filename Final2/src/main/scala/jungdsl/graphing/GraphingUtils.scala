package jungdsl.graphing

import org.apache.commons.collections15.Factory

import scala.language.implicitConversions

import edu.uci.ics.jung.graph.Graph
import scala.collection._

/**
 * The purpose of this trait is to provide all the necessary features that
 * are required in order to use the graph creation capabilities of the DSL
 * (adding vertices and edges and so on...)
 *
 * This trait is specifically meant to be mixed in the 'root' dsl package-
 * object. This way the end-user will only need to import the package 'jungdsl'
 * in order to benefit from all the features of the language.
 */
trait GraphingUtils extends Factories {
  /**
   * This map stores all the GraphOps that have been converted from a graph.
   * This way, the system is able to retrieve the appropriate instance of the
   * GraphOps and return it when needed. The rationale being to not loose all
   * the configuration that have been made to the vertices and edges
   */
  val graphs = mutable.Map.empty[Graph[_,_],GraphOps[_,_]]

  /**
   * This implicit conversion lets us convert any object into an edge source.
   * This is particularly useful in order to create and add edges to some graph
   * @see EdgeSource
   * @param v the object to use as source of the edge
   * @tparam V the type of the object to convert
   * @return an EdgeSource[V] whose starting point is v
   */
  implicit def any2edgesource[V](v:V) = new EdgeSource[V](v)

  /**
   * This implicit conversion lets us convert any object into a vertex. This is
   * particularly useful in order to add vertices to some graph.
   * @param  v the value of the vertex to be created
   * @tparam V the type of the value held by the vertex to create
   * @return a new VertexOps that holds v as value
   */
  implicit def any2vertexops[V](v:V) = new VertexOps[V](v)

  /**
   * This implicit conversion lets us obtain a GraphOps instance corresponding to
   * an actual JungGraph. This method starts looking up the graphs map in order to
   * determine whether the GraphOps must be created or if it must be fetched from
   * the graphs map.
   * @param g the graph that needs to be 'converted'
   * @param factory the edge factory to use in case the graph needs to be created.
   *                This factory is necessary in order to let a user create edges
   *                with no explicit value.
   * @tparam V the type of the vertices in the graph
   * @tparam E the type of the edges in the graph
   * @return an instance of GraphOps that contains all the configurations that have
   *         been applied to the graph g.
   */
  implicit def graph2graphops[V,E](g:Graph[V,E])(implicit factory:Factory[E]) = graphs get g match {
    case Some(o) => o.asInstanceOf[GraphOps[V,E]]
    case None    => {
      val v = GraphOps[V, E](g)(factory)
      graphs += g -> v
      v
    }
  }
}
