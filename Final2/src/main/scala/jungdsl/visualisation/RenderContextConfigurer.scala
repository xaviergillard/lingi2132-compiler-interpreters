package jungdsl.visualisation

import edu.uci.ics.jung.visualization.RenderContext
import jungdsl.graphing.GraphOps
import org.apache.commons.collections15.Transformer

import scala.collection._
import scala.language.implicitConversions
import scala.reflect.ClassTag

/**
 * The purpose of this object is simply to extract and factorise the pretty long configuration
 * we need to do in order to let all the VertexOps and GraphOps customisations show on screen.
 */
object RenderContextConfigurer {
  /**
   * This method returns a new transformer that will: start looking up the map and then applies
   * the customisation if one is found, or defaults to the default transformation if no configuration
   * could be found.
   */
  private def transformer[I,T,O](map:mutable.Map[I,T])(extract: T=>O)(default:Transformer[I,O]) = new Transformer[I,O] {
    def transform(e:I) = map.get(e) match {
      case None                        => if(default!=null)default.transform(e) else null.asInstanceOf[O]
      case Some(o) if extract(o)==null => if(default!=null)default.transform(e) else null.asInstanceOf[O]
      case Some(o)                     => extract(o)
    }
  }

  /**
   * This method overwrite all the xxxTransformer of the RenderContext in such a way that they reflect
   * all the customisations that have been brought to the various item.
   * @param graph the GraphOps that needs to be displayed
   * @param ctx the RenderContext that will be used in order to display the graph on screen.
   * @tparam V the type of the vertices
   * @tparam E the type of the edges
   */
  def configure[V:ClassTag,E:ClassTag](graph:GraphOps[V,E], ctx: RenderContext[V,E]) {
    // memorize the default transformers
    val defaultArrowDraw = ctx.getArrowDrawPaintTransformer()
    val defaultArrowFill = ctx.getArrowFillPaintTransformer()
    val defaultArrowStroke = ctx.getEdgeArrowStrokeTransformer()

    val defaultEdgeDraw = ctx.getEdgeDrawPaintTransformer()
    val defaultEdgeFill = ctx.getEdgeFillPaintTransformer()
    val defaultEdgeStroke = ctx.getEdgeStrokeTransformer()
    val defaultEdgeFont = ctx.getEdgeFontTransformer()
    val defaultEdgeLabel = ctx.getEdgeLabelTransformer()

    val defaultVertexDraw = ctx.getVertexDrawPaintTransformer()
    val defaultVertexFill = ctx.getVertexFillPaintTransformer()
    val defaultVertexStroke = ctx.getVertexStrokeTransformer()
    val defaultVertexShape = ctx.getVertexShapeTransformer()
    val defaultVertexFont = ctx.getVertexFontTransformer()
    val defaultVertexLabel = ctx.getVertexLabelTransformer()
    val defaultVertexIcon = ctx.getVertexIconTransformer()

    // Arrow
    ctx.setArrowDrawPaintTransformer (
      transformer(graph.edges)(_.arrow.draw)(defaultArrowDraw)
    )
    ctx.setArrowFillPaintTransformer(
      transformer(graph.edges)(_.arrow.fill)(defaultArrowFill)
    )
    ctx.setEdgeArrowStrokeTransformer(
      transformer(graph.edges)(_.arrow.stroke)(defaultArrowStroke)
    )

    // Edge body
    ctx.setEdgeDrawPaintTransformer(
      transformer(graph.edges)(_.draw)(defaultEdgeDraw)
    )
    ctx.setEdgeFillPaintTransformer(
      transformer(graph.edges)(_.fill)(defaultEdgeFill)
    )
    ctx.setEdgeStrokeTransformer(
      transformer(graph.edges)(_.stroke)(defaultEdgeStroke)
    )
    ctx.setEdgeFontTransformer(
      transformer(graph.edges)(_.font)(defaultEdgeFont)
    )
    ctx.setEdgeLabelTransformer(
      transformer(graph.edges)(_.label)(defaultEdgeLabel)
    )

    //Vertex
    ctx.setVertexDrawPaintTransformer(
      transformer(graph.vertices)(_.draw)(defaultVertexDraw)
    )
    ctx.setVertexFillPaintTransformer(
      transformer(graph.vertices)(_.fill)(defaultVertexFill)
    )
    ctx.setVertexStrokeTransformer(
      transformer(graph.vertices)(_.stroke)(defaultVertexStroke)
    )
    ctx.setVertexFontTransformer(
      transformer(graph.vertices)(_.font)(defaultVertexFont)
    )
    ctx.setVertexLabelTransformer(
      transformer(graph.vertices)(_.label)(defaultVertexLabel)
    )
    ctx.setVertexShapeTransformer(
      transformer(graph.vertices)(_.shape)(defaultVertexShape)
    )
    ctx.setVertexIconTransformer(
      transformer(graph.vertices)(_.icon)(defaultVertexIcon)
    )
  }
}
