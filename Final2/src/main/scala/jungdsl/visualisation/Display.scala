package jungdsl.visualisation

import java.awt.Dimension
import javax.swing.{JComponent, JFrame, JMenuBar}

import edu.uci.ics.jung.algorithms.layout.Layout
import edu.uci.ics.jung.graph.Graph
import edu.uci.ics.jung.visualization._
import edu.uci.ics.jung.visualization.control.{DefaultModalGraphMouse, EditingModalGraphMouse}
import jungdsl.customization.{Layouts, SideEffect}
import jungdsl.graphing.GraphOps
import org.apache.commons.collections15.Factory

import scala.reflect.ClassTag

/**
 * This object is the enty-point to the visualisation sub-DSL.
 * Thanks to this object we can write sentences such as:
 *
 *     Display(graph) withDimension (200 x 300) withLayout Circle inEditMode
 *
 * which will result in the graph to be shown in a swing view
 */
object Display {
  /**
   * This instanciates the OngoingViewBuild that is meant to configure and produce the view to be show
   * @param graph the graph to display
   * @tparam V the type of the vertices
   * @tparam E the type of the edges
   * @return an instance of OngoingViewBuild that lets you configure and create the view to be shown
   */
  def apply[V:ClassTag,E:ClassTag](graph: GraphOps[V,E]) = new OngoingViewBuild(graph)

  /**
   * This internal class represents the state of a graph view under construction. It provides the
   * necessary functions to customize and display the graph on screen.
   * @param graph the graph to be shown in the end
   * @tparam V the type of the vertices
   * @tparam E the type of the edges
   */
  class OngoingViewBuild[V:ClassTag,E:ClassTag](graph: GraphOps[V,E])
    extends SideEffect[OngoingViewBuild[V,E]]
    with Layouts {
    /**
     * This is just an alias for the type of the functions that construct a layout
     * from a graph. This is only meant to alleviate the writing.
     */
    type LayoutConstructor = Graph[V,E] => Layout[V, E]

    /** this is the layout to use when building the view */
    private var layout    = Circle[V,E]
    /** this is the dimension of the window to be used when showing the view */
    private var dimension = new Dimension(300,300)

    /** this function lets you customise the layout to use with the one passed as param */
    def withLayout(alt: LayoutConstructor) = sideEffect(layout = alt)
    /** this function lets you customize the dimention of the window to be produced */
    def withDimension(dim: Dimension)      = sideEffect(dimension=dim)

    /**
     * This method lets you display what you have configured in a swing window. The displayed
     * view will have the necessary capabilities to rotate, skew, ... the graph
     * BUT NOT TO EDIT IT IN ANY WAY. If edition is what you are looking for, use the method
     * inEditMode.
     */
    def inReadOnlyMode {
      val view = new VisualizationViewer[V,E](layout(graph.graph), dimension)
      RenderContextConfigurer.configure(graph, view.getRenderContext)
      view.setGraphMouse(new DefaultModalGraphMouse[V,E]())

      val frame= rootFrame(view)
      frame.pack()
      frame.setVisible(true)
    }

    /**
     * This method lets you display what you have configured in a swing window. The displayed
     * view will offer all the necessary capabilities to interactively build up the graph.
     */
    def inEditMode(implicit fV:Factory[V], fE:Factory[E]) {
      val view = new VisualizationViewer[V,E](layout(graph.graph), dimension)
      val mouse= new EditingModalGraphMouse[V,E](view.getRenderContext, fV, fE)

      RenderContextConfigurer.configure(graph, view.getRenderContext)
      view.setGraphMouse(mouse)

      val frame= rootFrame(view)

      frame.setJMenuBar(menu(mouse))

      frame.pack()
      frame.setVisible(true)
    }

    /** this method creates a menu to control the behavior of the mouse */
    private def menu(mouse:EditingModalGraphMouse[V,E]) = {
      val menu = new JMenuBar()
      val modeMenu = mouse.getModeMenu
      modeMenu.setText("Mouse Mode")
      modeMenu.setIcon(null)
      modeMenu.setPreferredSize(new Dimension(100,20))
      menu.add(modeMenu)
      menu
    }
    /** this method creates the root frame to show */
    private def rootFrame(view:JComponent) = {
      val frame = new JFrame()
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
      frame.getContentPane.add(view)
      frame
    }
  }
}