package jungdsl.customization

import edu.uci.ics.jung.algorithms.layout._
import edu.uci.ics.jung.graph.Graph

import scala.reflect.{ClassTag, _}

/**
 * This trait provides a convenient way to create new instances of a layout that
 * are appropriate to display the graph at hand.
 */
trait Layouts {
  /**
   * This makes use of the scala reflec api to alleviate the notation and
   * return an invocation of the constructor.
   *
   * @param graph the graph for which to construct a layout
   * @tparam V    the type of the vertices from the graph
   * @tparam T    the type of the layout to create
   * @return      an invocation of the layout constructor for the given graph
   */
  private def constr[V, E, T:ClassTag](graph:Graph[V,E]) =
    classTag[T].runtimeClass.getConstructor(classOf[Graph[V,E]]).
    newInstance(graph).
    asInstanceOf[Layout[V,E]]

  // NOTE: All the invocations are curried, that makes the usage feel more natural

  /** @see CircleLayout#CircleLayout(Graph[V,E] g) */
  def Circle[V,E]               = constr[V, E, CircleLayout [V, E]]_
  /** @see DAGLayout#DAGLayout(Graph[V,E] g) */
  def DirectedAcyclic[V,E]      = constr[V, E, DAGLayout    [V, E]]_
  /** @see FRLayout#FRLayout(Graph[V,E] g) */
  def FruchtermanReingold[V,E]  = constr[V, E, FRLayout     [V, E]]_
  /** @see FRLayout2#FRLayout2(Graph[V,E] g) */
  def FruchtermanReingold2[V,E] = constr[V, E, FRLayout2    [V, E]]_
  /** @see ISOMLayout#ISOMLayout(Graph[V,E] g) */
  def SelfOrganizing[V, E]      = constr[V, E, ISOMLayout   [V, E]]_
  /** @see KKLayout#KKLayout(Graph[V,E] g) */
  def KamadaKawai[V, E]         = constr[V, E, KKLayout     [V, E]]_
  /** @see SpringLayout#SpringLayout(Graph[V,E] g) */
  def Spring[V, E]              = constr[V, E, SpringLayout [V, E]]_
  /** @see SpringLayout2#SpringLayout2(Graph[V,E] g) */
  def Spring2[V, E]             = constr[V, E, SpringLayout2[V, E]]_
  /** @see StaticLayout#StaticLayout(Graph[V,E] g) */
  def Static[V, E]              = constr[V, E, StaticLayout [V, E]]_
}
