package jungdsl.customization

import java.awt.Color._

/**
 * This trait provides a convenient set of constants that help the user define
 * the colors to use to draw and fill the items.
 */
trait Colors {
  val White     = WHITE
  val LightGray = LIGHT_GRAY
  val Gray      = GRAY
  val DarkGray  = DARK_GRAY
  val Black     = BLACK
  val Red       = RED
  val Pink      = PINK
  val Orange    = ORANGE
  val Yellow    = YELLOW
  val Green     = GREEN
  val Mahgenta  = MAGENTA
  val Cyan      = CYAN
  val Blue      = BLUE
}

/**
 * This object serves as a convenient RGB color factory
 */
object Color {
  /** This method creates a new RGB color */
  def apply(r:Int, g:Int, b:Int) = new java.awt.Color(r,g,b)
}
