package jungdsl.customization

import java.awt.Dimension

/**
 * The purpose of this trait is to provide all the necessary features that
 * are required in order to use the customisation capabilities of the DSL
 * (edges+arrow and vertices colors, stokes etc..)
 *
 * This trait is specifically meant to be mixed in the 'root' dsl package-
 * object. This way the end-user will only need to import the package 'jungdsl'
 * in order to benefit from all the features of the language.
 */
trait CustomisationUtils extends Layouts with Strokes with Shapes with Colors {

  /**
   * The sole purpose of this implicit class, is to permit the user to
   * write Dimensions in a more natural way. Namely, it allows the user to
   * write something like 200 x 300 and obtain a proper Dimension object.
   *
   * @param a this is the first term of the dimension (width).
   */
  implicit class OngoingDimensionBuild(a:Int) {
    /**
     * This method actually constructs a dimension using the given terms
     * @param b this is the second term of the dimension (height)
     * @return
     *    a proper dimension object that corresponds to the given width
     *    and height
     */
    def x(b:Int) = new Dimension(a, b)
  }

}
