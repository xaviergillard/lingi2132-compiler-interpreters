package jungdsl.customization

/**
 * This file contains all the traits related to the visual customisation of
 * the graph edges(+arrow) and vertices.
 *
 * The purpose of all these traits is to be mixed in together to add the
 * capability they define onto some other concrete objects.
 */

import java.awt._
import javax.swing.Icon

/**
 * This trait defines the higher-order 'sideEffect' function that lets us define
 * the customisations to be performed in terms of the side effect embodied by that
 * customisation. Thanks to this, we are able to define customisations that can
 * be chained together and all have their effect applied.
 * @tparam T the type of object to be retured by the sideEffect function. Typically
 *           it needs to be the (reifiable/concrete) type of the class that mixes-in
 *           this trait. It is at the very heart of the chaining mechanism.
 */
trait SideEffect[T] {
  /**
   * This function executes the side effect embodied in the by-name f and then returns
   * an instance of the object upon which this object was called. (That's how the
   * chaining is made possible).
   *
   * @param f the by-name computation that needs to be applied
   * @return  a reference to the object upon which this side effect was applied.
   */
  def sideEffect(f: => Any) = {f; this.asInstanceOf[T]}
}

/**
 * This trait embodies all the features related to the basic visual customisations
 * that can be applied on an item. (It basically goes about draw, fill and stroke
 * properties).
 *
 * NOTE: This trait is constrained with a self-type that forces this trait to be
 *  mixed in with AT LEAST SideEffect[T]. The reason why we opted for this approach
 *  instead of letting this trait inherit from SideEffect[T] was to avoid multiple
 *  definitions of the sideEffect method.
 *
 * @tparam T this is the type this object is mixed in with
 */
trait VisualCustomisation[T] {
  self:SideEffect[T] =>
  /** this property controls the colour in which this item will be drawn (not filled) */
  var draw:Paint          = _
  /** this property controls the colour in which this item will be filled (not drawn) */
  var fill:Paint          = _
  /** this property controls the stroke to be applied to the drawing. */
  var stroke:Stroke       = _

  /** this method provides a chainable hook to control the drawing of this item */
  def drawIn(p:Paint)     = sideEffect(draw    = p)
  /** this method provides a chainable hook to control the filling of this item */
  def fillWith(p:Paint)   = sideEffect(fill    = p)
  /** this method provides a chainable hook to control the stroke of this item */
  def withStroke(s:Stroke)= sideEffect(stroke  = s)
}

/**
 * This trait embodies all the features related to the advanced visual customisations
 * that can be applied on an item. (Shape and icon properties).
 *
 * NOTE: This trait is constrained with a self-type that forces this trait to be
 *  mixed in with AT LEAST SideEffect[T]. The reason why we opted for this approach
 *  instead of letting this trait inherit from SideEffect[T] was to avoid multiple
 *  definitions of the sideEffect method.
 *
 * @tparam T this is the type this object is mixed in with
 */
trait AdvancedVisualCustomisation[T] extends VisualCustomisation[T] {
  self:SideEffect[T] =>
  /** this property controls the shape that will be used to represent this item when displayed */
  var shape:Shape         = _
  /** this property controls the icon that will be used to represent this item when displayed */
  var icon:Icon           = _

  /** this method provides a chainable hook to control the icon of this item */
  def withIcon(i:Icon)    = sideEffect( icon = i )
  /** this method provides a chainable hook to control the shape of this item */
  def withShape(s:Shape)  = sideEffect(shape   = s)
}

/**
 * This trait embodies all the features related to the advanced visual customisations
 * of those objects that can have an associated label.
 *
 * NOTE: This trait is constrained with a self-type that forces this trait to be
 *  mixed in with AT LEAST SideEffect[T]. The reason why we opted for this approach
 *  instead of letting this trait inherit from SideEffect[T] was to avoid multiple
 *  definitions of the sideEffect method.
 *
 * @tparam T this is the type this object is mixed in with
 */
trait LabelCustomisation[T] {
  self:SideEffect[T] =>
  /** this property controls the font that will be used to write the label */
  var font:Font           = _
  /** this property controls the text of the associated label */
  var label:String        = _

  /** this method provides a chainable hook to control the font of this item */
  def withFont(f:Font)    = sideEffect(font    = f)
  /** this method provides a chainable hook to control the label-text of this item */
  def withLabel(l:String) = sideEffect(label   = l)
}

/**
 * This trait embodies all the features related to the visual customisations of the
 * arrowhead of an item (typically a directed edge).
 *
 * NOTE: This trait is constrained with a self-type that forces this trait to be
 *  mixed in with AT LEAST SideEffect[T]. The reason why we opted for this approach
 *  instead of letting this trait inherit from SideEffect[T] was to avoid multiple
 *  definitions of the sideEffect method.
 *
 * @tparam T this is the type this object is mixed in with
 */
trait ArrowHeadCustomisation[T] {
  self:SideEffect[T] =>
  /** this gives a hook to the customisation of the arrowhead itself */
  object arrow extends VisualCustomisation[T] with SideEffect[T]

  /** this method provides a chainable hook to control the way the arrowhead will be drawn */
  def drawArrowIn(p:Paint)       = sideEffect(arrow.draw   = p)
  /** this method provides a chainable hook to control the way the arrowhead will be filled */
  def fillArrowIn(p:Paint)       = sideEffect(arrow.fill   = p)
  /** this method provides a chainable hook to control the way the arrowhead will be stroked */
  def withArrowStroke(s:Stroke)  = sideEffect(arrow.stroke = s)
}