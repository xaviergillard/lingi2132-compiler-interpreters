package jungdsl.customization

import java.awt.BasicStroke

import edu.uci.ics.jung.visualization.RenderContext

/**
 * This trait provides a convenient set of constants that help the user define
 * the stroke he/she wants to apply to some given item.
 */
trait Strokes {
  /** use this one if you want the stroke to be dashed eg: - - - - */
  val Dashed = RenderContext.DASHED
  /** use this one if you want the stroke to be dotted eg: ....... */
  val Dotted = RenderContext.DOTTED
  /** use this one if you want the line to be thick */
  val Thick  = new BasicStroke(5)
}
