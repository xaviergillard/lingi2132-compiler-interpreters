package jungdsl.customization

import java.awt.Rectangle
import java.awt.geom.Ellipse2D

/**
 * This trait provides a convenient way to create new instances of some common shapes.
 * It is particularly useful to the user to configure the shape of (ie) a vertex.
 *
 * NOTE: an alternative option was, instead of defining methods dedicated to the creation
 * of the shapes; to define companion objects for the corresponding awt classes that would
 * all have an apply method in order to create the desired instance. Although elegant, this
 * approach was considered too verbose for the purpose at hand and therefore discarded.
 */
trait Shapes {
  /** creates a new square shape */
  def Square(side:Int) = new Rectangle(side, side)
  /** creates a new rectangular shape */
  def Rectangular(l:Int, L:Int) = new Rectangle(l, L)
  /** creates a new circular shape */
  def Circular(radius:Float) = new Ellipse2D.Float(-radius/2, -radius/2, radius, radius)
}
