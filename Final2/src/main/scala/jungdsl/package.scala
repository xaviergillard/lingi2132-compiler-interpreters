import jungdsl.customization.CustomisationUtils
import jungdsl.graphing.{GraphOps, GraphingUtils}

/**
 * This package object lets you bring all the feature of the jung DSL in your scope at once.
 * Since it was implemented by stacking the main traits of the different subpackages that compose
 * the DSL, it is sufficient to only import jungdsl to get all of the DSL magic in your scope.
 */
package object jungdsl extends GraphingUtils with CustomisationUtils {
  /*
   * These val are declared here in order to let the client be unaware of any place where these
   * objects are defined. This way, all the end-user needs to care about is the name of these objects.
   */
  val Graph  = GraphOps
  val Display= jungdsl.visualisation.Display
  val Color  = jungdsl.customization.Color
}
