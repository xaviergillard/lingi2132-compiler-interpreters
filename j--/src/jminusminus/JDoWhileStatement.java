package jminusminus;

import static jminusminus.CLConstants.*;

/**
 * This class represents an AST node depicting a do-while statement
 */
public class JDoWhileStatement extends JStatement {
	/** the body statement */
	private JStatement body;
	/** the continuation condition */
	private JExpression condition;
	
	/**
	 * Constructs the AST node
	 * @param line the line
	 * @param condition the continuation condition
	 * @param body the statement (body) to be executed
	 */
	public JDoWhileStatement(int line, JExpression condition, JStatement body) {
		super(line);
		this.condition = condition;
        this.body = body;
	}
	
	/** {@inheritDoc} */
	public JAST analyze(Context context) {
		condition.analyze(context);
		condition.type().mustMatchExpected(line(), Type.BOOLEAN);
		body.analyze(context);
		return this;
	}

	/** {@inheritDoc} */
	public void codegen(CLEmitter output) {
		String labelStart = output.createLabel();
		output.addLabel(labelStart);
		body.codegen(output);
		condition.codegen(output, labelStart, true);
	}

	/** {@inheritDoc} */
	public void writeToStdOut(PrettyPrinter p) {
		p.printf("<JDoWhileStatement line=\"%d\">\n", line());
        p.indentRight();
        p.printf("<Body>\n");
        p.indentRight();
        body.writeToStdOut(p);
        p.indentLeft();
        p.printf("</Body>\n");
        p.printf("<TestExpression>\n");
        p.indentRight();
        condition.writeToStdOut(p);
        p.indentLeft();
        p.printf("</TestExpression>\n");
        p.indentLeft();
        p.printf("</JDoWhileStatement>\n");
	}

}
