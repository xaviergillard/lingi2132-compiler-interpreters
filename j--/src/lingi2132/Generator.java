package lingi2132;

import java.util.ArrayList;

import jminusminus.CLEmitter;
import static jminusminus.CLConstants.*;
/**
 * This class implements the 2nd half of the 1st part of the 
 * "languages and translators" assignment.
 * 
 * Here the point is to generate bytecode almost manually.
 */
public class Generator extends FibonacciGenerator {
	
	/**
	 * creates a new instance.
	 * @param outputDir the output directory 
	 */
	public Generator(String outputDir) {
		super(outputDir);
	}
	
	/** {@inheritDoc} */
	public void generateClass() {
		CLEmitter out = new CLEmitter(true);
		out.destinationDir(outputDir);
		
		ArrayList<String> modifiers = new ArrayList<String>();
		modifiers.add("public");
		
		String thisClass = "packageOfClassToGenerate/ClassToGenerate";
		String superType = "java/lang/Object";	
		String construct = "<init>";
		String params    = "()V";
		
		out.addClass(modifiers, thisClass, superType, null, false);
		
		// add default constructor
		out.addMethod(modifiers, construct, params, null, false);
		out.addNoArgInstruction(ALOAD_0);	// Load this
		out.addMemberAccessInstruction(INVOKESPECIAL, superType, construct, params);
		// ain't no field to init
		out.addNoArgInstruction(RETURN);
		
		// add the fibonacci method
		modifiers = new ArrayList<String>();
		modifiers.add("public");
		modifiers.add("static");
		
		String elseLabel= out.createLabel();
		
		out.addMethod(modifiers, "fibonacci", "(I)I", null, false);
		out.addNoArgInstruction(ILOAD_0);	// because it's static
		out.addNoArgInstruction(ICONST_2);
		out.addBranchInstruction(IF_ICMPGE, elseLabel);
		out.addNoArgInstruction(ILOAD_0);
		out.addNoArgInstruction(IRETURN);	
		
		out.addLabel(elseLabel);
		out.addNoArgInstruction(ILOAD_0);	// fibo(n-1)
		out.addNoArgInstruction(ICONST_1);
		out.addNoArgInstruction(ISUB);
		out.addMemberAccessInstruction(INVOKESTATIC, thisClass, "fibonacci", "(I)I");
		
		out.addNoArgInstruction(ILOAD_0);	// fibo(n-2)
		out.addNoArgInstruction(ICONST_2);
		out.addNoArgInstruction(ISUB);
		out.addMemberAccessInstruction(INVOKESTATIC, thisClass, "fibonacci", "(I)I");
		
		out.addNoArgInstruction(IADD);      // fibo(n-1) + fibo(n-2)
		out.addNoArgInstruction(IRETURN);
		
		out.write();
	}

}
