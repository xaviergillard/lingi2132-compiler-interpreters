package lingi2132;

import junit.framework.TestCase;
import junit.framework.Assert;

import packageOfClassToGenerate.ClassToGenerate;

/**
 * Please note that in order to let this test run properly, the ant target 'manualGeneration'
 * should be run. 
 */
public class TestManuallyGeneratedCode extends TestCase {
	public void testFiboZeroIsZero(){
		Assert.assertEquals(0, ClassToGenerate.fibonacci(0));
	}
	public void testFiboOneIsOne(){
		Assert.assertEquals(1, ClassToGenerate.fibonacci(1));
	}
	public void testFiboTwoIsOne(){
		Assert.assertEquals(1, ClassToGenerate.fibonacci(2));
	}
	public void testFiboThreeIsTwo(){
		Assert.assertEquals(2, ClassToGenerate.fibonacci(3));
	}
	public void testFiboFourIsThree(){
		Assert.assertEquals(3, ClassToGenerate.fibonacci(4));
	}
	public void testFiboFiveIsFive(){
		Assert.assertEquals(5, ClassToGenerate.fibonacci(5));
	}
	public void testFiboSixIsEight(){
		Assert.assertEquals(8, ClassToGenerate.fibonacci(6));
	}
}
