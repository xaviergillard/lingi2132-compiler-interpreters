package pass;

// This class showcases the proper use of the do-while statement
public class DoWhile {
	
	// must return 1
	public int itMustExecuteStatementAtLeastOnce(){
		int i = 0;
		do i=inc(i); while(false);
		return i;
	}
	
	// must return 3
	public int itMustExecuteStatementAsLongAsConditionIsSatisfied(){
		int i = 0;
		do i=inc(i); while(i<=2);
		return i;
	}
	
	// This is only syntax validation
	public void itMayContainNothingButAWhileLoop(){
		do while(true); while(true);
	}
	
	// Just a joke if you read the code ;-)
	public void itMaySingAlongWithThePolice(){
		do do do ;
		// da da da !; 
		while(true); while(true); while(true);
	}
	
	// This is only a utility method
	private int inc(int i){ return i+1; }
}
