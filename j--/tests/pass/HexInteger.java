package pass;

//This class showcases the different ways the hex integer notation can be used in a j-- 
//source code.
public class HexInteger {
	
	// must return 15
	public int prefixMayBeLowerCase(){
		return 0xf;
	}
	
	// must return 15
	public int prefixMayBeUpperCase(){
		return 0Xf;
	}
	
	// must return 11259375
	public int tailCanBeUpperCase(){
		return 0XABCDEF;
	}
	// must return 11259375
	public int tailCanBeLowerCase(){
		return 0Xabcdef;
	}
	// must return 11259375
	public int tailCanMixCases(){
		return 0xAbCdEf;
	}
	// must return 1715004
	public int tailCanMixAlphaAndNumericDigits(){
		return 0x1a2B3c;
	}
	// must return 255
	public int tailCanStartWithLeadingZero(){
		return 0x0FF;
	}
}
