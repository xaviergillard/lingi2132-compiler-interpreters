package pass;

// This class showcases the different ways the octal integer notation can be used
// in a j-- source file.
public class OctInteger {
	
	// must return 1
	public int itMayStartWithLeadingZeroes(){
		return 0001;
	}
	
	// must return 342391
	public int itMayContainDigitsUpToSeven(){
		return 01234567;
	}

	// must return 2739128
	public int zeroIsAValidDigit(){
		return 012345670;
	}
}
