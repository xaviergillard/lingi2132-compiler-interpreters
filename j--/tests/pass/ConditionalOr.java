package pass;

// This class showcases the way a conditional or should be used 
// in a j-- source file
public class ConditionalOr {
	// must return true
	public boolean itMustBeTrueWhenFirstOperandIsTrue(){
		return true||false;
	}
	// must return true
	public boolean itMustBeTrueWhenSencondOperandIsTrue(){
		return true||true;
	}
	// must return true
	public boolean itMustBeTrueWhenBothOperandsAreTrue(){
		return true||true;
	}
	// must return false
	public boolean itMustBeFalseWhenBothOperandsAreFalse(){
		return false||false;
	}
	// must return true and not crash
	public boolean itMustShortCircuitSecondOperandIfPossible(){
		String s = null;
		return true || s.equals((Object)"Bonjour");
	}
	// must return true
	public boolean itMayBeChained(){
		return true||true||true||false;
	}
	// must return true and not crash
	public boolean itMustAlsoShortCircuitWhenChained(){
		String s = null;
		return true||false||false||s.equals((Object)"Anything")||true;
	}
	//must return true;
	public boolean itMayBeMixedWithConditionalAnd(){
		return true&&false||true;
	}
	//must return false
	public boolean itMustBeLessPrecedentThanConditionalAnd(){
		return false||true&&false;
	}
}
