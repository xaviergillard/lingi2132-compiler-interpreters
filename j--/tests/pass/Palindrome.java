package pass;

// j-- seems to not import the classes from java.lang by default.
import java.lang.StringBuilder;
import java.lang.System;

// This class defines the features to test whether a given input string
// is a palindrome or not
public class Palindrome {
	
	// This is the program entry point, args are the command-line arguments
	public static void main(String[] args){
		if(! (args.length == 1) ){
			die("Usage:\n\tjava pass.Palindrome <an_input_string>");
		}
		
		System.out.println(new Palindrome().palindrome(args[0]));
	}
	
	// This method returns s when s is a palindrome
	// otherwise it returns the empty string
	public String palindrome (String s) {
		if(isPalindrome(s)){
			return s;
		} 
		return "";
	}
	
	// Returns true when s is a palindrome, false otherwise
	// note, null (and blank string) is considered to be a palindrome
	private boolean isPalindrome(String s){
		if((Object) s == null) {
			return true;
		}
		String normalized = s.toLowerCase();
		StringBuilder sb = new StringBuilder(normalized);
		
		// This implementation is suboptimal, it must be possible to
		// implement this test comparing the characters only once iso 2
		String reversed = sb.reverse().toString();
		return ((Object)reversed).equals((Object) normalized);
	}
	
	// Terminates the program displaying a explanatory message
	private static void die(String message){
		System.err.println(message);
		// ERROR (I used to declare it as a private static final int constant but it 
		// is not supported by j--
		System.exit(1);
	}
}
