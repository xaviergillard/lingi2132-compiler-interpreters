package pass;

// This class showcases the different ways the bitwise flip (~) can be used in a j-- 
// source code.
public class BitwiseFlip {
	
	// must return -1
	public int termCanBeLitteral(){
		return ~0;
	}
	
	// must return 0
	public int termCanBeNegatedInt(){
		return ~-1;
	}
	
	// must return 1
	public int itHasTheSamePriorityAsUnaryMinus(){
		return -~0;
	}
	
	// must return -3
	public int itHasPrecedenceOverMultiplicativeExpressions(){
		return ~0*3;
	}
	
	// must return 3
	public int itIsReversible(){
		return ~~3;
	}
}
