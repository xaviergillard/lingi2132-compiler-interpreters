package pass;
//
// This class is a sample code that should pass compilation although it uses the 
// division operator that wasn't initially defined.
// 
// This code was copied over from the book 
// 	'Introduction to Compiler Construction in a Java World'
//  by Campbell, Iyer and Akbal-Delibas
//
public class Division {
	
	//
	// This method features the division operator.
	// (integer division)
	// 
	// @param x the numerator
	// @param y the denominator
	// @return the quotient of x divided by y using integer division.
	//
	public int divide(int x, int y){
		return x / y;
	}
	
	// ADDENDUM

	// This method showcases that the division should be able to cope with 'complex' 
	// (integer) expressions as terms of the division.
	// 
	// @return the value two
	//
	public int complexDivision(){
		return 8+4 / 8-2;
	}
}
