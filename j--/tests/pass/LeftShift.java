package pass;

import java.lang.Integer;

// This simple class features some examples of how the left shift operator can be used.
// Complete specification of this operator is to be found here: 
//		http://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.19
public class LeftShift {
	
	// must return 16
	public int membersCanBeLitteral(){
		return 2<<3;
	}
	
	// must return -4
	public int firstTermCanBeNegated(){
		return -1 << 2;
	}
	
	// must return 0
	public int secondTermCanBeNegated(){
		return Integer.MIN_VALUE<<-1;
	}
		
	// must return 16
	public int firstTermCanBeMultiplicativeExpr(){
		return 2*1<<3;
	}
	
	// must return 16
	public int secondTermCanBeMultiplicativeExpr(){
		return 2<<1*3;
	}
	
	// must return 16
	public int bothTermsCanBeMultiplicativeExpr(){
		return 2*1<<1*3;
	}
	
	// must return 16
	public int firstTermCanBeAdditiveExpr(){
		return 1+1<<3;
	}
	
	// must return 16
	public int secondTermCanBeAdditiveExpr(){
		return 2<<2+1;
	}
	
	// must return 16
	public int bothTermsCanBeAdditiveExpr(){
		return 1+1<<2+1;
	}
	
	// must return 8
	public int itCanBeChained(){
		return 2<<1<<1;
	}
	
	//
	// Extract from JLS:
	//  (Source: http://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.19)
	//
	// If the promoted type of the left-hand operand is int, only the five lowest-order bits of
	// the right-hand operand are used as the shift distance. It is as if the right-hand operand 
	// were subjected to a bitwise logical AND operator & (§15.22.1) with the mask value 0x1f (0b11111). 
	// The shift distance actually used is therefore always in the range 0 to 31, inclusive.
	// 
	// must return Integer.MIN_VALUE
	public int rightOperandIsBoundBetween0And31_negative(){
		return 1<<-1;
	}
	// must return 1
	public int rightOperandIsBoundBetween0And31_0(){
		return 1<<0;
	}
	// must return 1
	public int rightOperandIsBoundBetween0And31_32(){
		return 1<<32;
	}
}
