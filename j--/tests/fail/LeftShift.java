package fail;

// This class features syntactic and semantic deviations of the left shift operator
public class LeftShift {
	
	//
	// Syntactic verification
	//
	public int thereMustBeALeftOperand(){
		return <<3;
	}
	
	public int thereMustBeARightOperand(){
		return 2<<;
	}
	
	public int itMustSitBetweenOperands(){
		return 3 2 <<;
	}
	
	// 
	// Semantic verification
	//
	
	public int firstTermMustBeInt(){
		return '2'<<3;
	}
	
	public int secondTermMustBeInt(){
		return 2<<'3';
	}
	
	public int bothTermsMustBeInt(){
		return '2'<<'3';
	}
}
