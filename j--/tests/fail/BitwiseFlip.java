package fail;

// This class shows how the bitwise flip (~) aka one's complement CANNOT be used.
public class BitwiseFlip {
	
	public void itMustHaveAnOperand(){
		~;
	}
	
	public int itMustPreceedeItsOperand(){
		return 1~;
	}
	
	public int itIsAnUnaryOperator(){
		return 1~2;
	}
	
	public long itOperatesOnIntOnly(){
		return ~2L;
	}

}
