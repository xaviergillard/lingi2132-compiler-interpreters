package fail;

//
// J-- will not be able to compile this class because it violates the 
// constrtaint stating that j-- is only able to deal with integer types.
//
public class Division {
	
	// This could have been an entry point 
	public static void main(String[]args){
		// !! FAIL !!
		System.out.println("5 / 2.0 = "+ 5/2.0);
	}
}
