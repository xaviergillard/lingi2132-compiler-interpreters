package fail;

// This class showcases the way conditional or CANNOT be used in a 
// j-- source file
public class ConditionalOr {
	
	public boolean firstOperandMustBePresent(){
		return ||true;
	}
	public boolean seconOperandMustBePresent(){
		return true||;
	}
	public boolean bothOperandsMustBePresent(){
		return ||;
	}
	public boolean firstOperandMustBeBoolean(){
		return 3||false;
	}
	public boolean secondOperandMustBeBoolean(){
		return false||"";
	}
	public boolean bothOperandsMustBeBoolean(){
		return ""||"";
	}
}
