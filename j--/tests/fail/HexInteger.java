package fail;

// This class shows how the hex integer notation CANNOT be used.
public class HexInteger {
	
	public int prefixMustBeFollowedByTail(){
		return 0x;
	}
	
	public int tailMayNotContainNonHexUpperDigits(){
		return 0x12345GG;
	}
	
	public int tailMayNotContainNonHexLowerDigits(){
		return 0x12345gg;
	}
	
}
