package fail;

// This class showcases how not to use the do while instruction
public class DoWhile {
	
	public void thereMusteBeAstatement() {
		do while(true);
	}
	
	public void thereMustBeACondition() {
		do {} while;
	}
	
	public void thereMustBeASemicolonAtTheEnd() {
		do {} while(true)
	}
}
