package fail;

// This class showcases how the octal integer notation CANNOT be used.
public class OctInteger {
	
	public int itCannotContainAlphaDigit(){
		return 0a;
	}
	
	public int itCannotContainDigitGreaterThanSeven(){
		return 08;
	}
}
