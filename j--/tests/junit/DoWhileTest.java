package junit;

import pass.DoWhile;
import junit.framework.Assert;
import junit.framework.TestCase;

// Verifies that the do while loop behaves as expected
public class DoWhileTest extends TestCase {
	/** this is the tested object */
	private DoWhile tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new DoWhile();
	}
	
	// must return 1
	public void testItMustExecuteStatementAtLeastOnce(){
		Assert.assertEquals(1, tested.itMustExecuteStatementAtLeastOnce());
	}
	
	// must return 3
	public void testItMustExecuteStatementAsLongAsConditionIsSatisfied(){
		Assert.assertEquals(3, tested.itMustExecuteStatementAsLongAsConditionIsSatisfied());
	}
}
