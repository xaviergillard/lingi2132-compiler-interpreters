package junit;

import pass.HexInteger;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This class verifies that the compiled code indeed satifies its
 * intended meaning.
 */
public class HexIntegerTest extends TestCase {
	/** this is the tested object */
	private HexInteger tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new HexInteger();
	}
	
	
	// must return 15
	public void testPrefixMayBeLowerCase(){
		Assert.assertEquals(15, tested.prefixMayBeLowerCase());
	}
	
	// must return 15
	public void tesPrefixMayBeUpperCase(){
		Assert.assertEquals(15, tested.prefixMayBeUpperCase());
	}
	
	// must return 11259375
	public void testTailCanBeUpperCase(){
		Assert.assertEquals(11259375, tested.tailCanBeUpperCase());
	}
	// must return 11259375
	public void testTailCanBeLowerCase(){
		Assert.assertEquals(11259375, tested.tailCanBeLowerCase());
	}
	// must return 11259375
	public void testTailCanMixCases(){
		Assert.assertEquals(11259375, tested.tailCanMixCases());
	}
	// must return 1715004
	public void testTailCanMixAlphaAndNumericDigits(){
		Assert.assertEquals(1715004, tested.tailCanMixAlphaAndNumericDigits());
	}
	// must return 255
	public void testTailCanStartWithLeadingZero(){
		Assert.assertEquals(255, tested.tailCanStartWithLeadingZero());
	}
	
}
