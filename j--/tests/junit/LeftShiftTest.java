package junit;

import junit.framework.Assert;
import junit.framework.TestCase;
import pass.LeftShift;

/**
 * This class verifies that the code compiled using the left shift operator
 * that we freshly defined indeed has the intended meaning.
 */
public class LeftShiftTest extends TestCase {
	/** this is the tested object */
	private LeftShift tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new LeftShift();
	}
	
	public void testMembersCanBeLiteral(){
		Assert.assertEquals(16, tested.membersCanBeLitteral());
	}
	
	public void testFirstTermCanBeNegated(){
		Assert.assertEquals(-4, tested.firstTermCanBeNegated());
	}
	
	public void testSecondTermCanBeNegated(){
		Assert.assertEquals(0, tested.secondTermCanBeNegated());
	}
	
	public void testCanBeMultiplicativeExpr(){
		Assert.assertEquals(16, tested.firstTermCanBeMultiplicativeExpr());
		Assert.assertEquals(16, tested.secondTermCanBeMultiplicativeExpr());
		Assert.assertEquals(16, tested.bothTermsCanBeMultiplicativeExpr());
	}
	
	public void testCanBeAdditiveExpr(){
		Assert.assertEquals(16, tested.firstTermCanBeAdditiveExpr());
		Assert.assertEquals(16, tested.secondTermCanBeAdditiveExpr());
		Assert.assertEquals(16, tested.bothTermsCanBeAdditiveExpr());
	}
	
	public void testItCanBeChained(){
		Assert.assertEquals(8, tested.itCanBeChained());
	}
	
	public void testRightOperandIsBoundBetween0And31(){
		Assert.assertEquals(Integer.MIN_VALUE, tested.rightOperandIsBoundBetween0And31_negative());
		Assert.assertEquals(1, tested.rightOperandIsBoundBetween0And31_0());
		Assert.assertEquals(1, tested.rightOperandIsBoundBetween0And31_32());
	}
}
