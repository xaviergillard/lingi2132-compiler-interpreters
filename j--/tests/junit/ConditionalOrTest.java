package junit;

import pass.ConditionalOr;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This class verifies that the compiled code indeed satifies its
 * intended meaning.
 */
public class ConditionalOrTest extends TestCase {
	/** this is the tested object */
	private ConditionalOr tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new ConditionalOr();
	}
	
	// must return true
	public void testItMustBeTrueWhenFirstOperandIsTrue(){
		Assert.assertTrue(tested.itMustBeTrueWhenFirstOperandIsTrue());
	}
	// must return true
	public void testItMustBeTrueWhenSencondOperandIsTrue(){
		Assert.assertTrue(tested.itMustBeTrueWhenSencondOperandIsTrue());
	}
	// must return true
	public void testItMustBeTrueWhenBothOperandsAreTrue(){
		Assert.assertTrue(tested.itMustBeTrueWhenBothOperandsAreTrue());
	}
	// must return false
	public void testItMustBeFalseWhenBothOperandsAreFalse(){
		Assert.assertFalse(tested.itMustBeFalseWhenBothOperandsAreFalse());
	}
	// must return true and not crash
	public void testItMustShortCircuitSecondOperandIfPossible(){
		Assert.assertTrue(tested.itMustShortCircuitSecondOperandIfPossible());
	}
	// must return true
	public void testItMayBeChained(){
		Assert.assertTrue(tested.itMayBeChained());
	}
	// must return true and not crash
	public void testItMustAlsoShortCircuitWhenChained(){
		Assert.assertTrue(tested.itMustAlsoShortCircuitWhenChained());
	}
	//must return true;
	public void testItMayBeMixedWithConditionalAnd(){
		Assert.assertTrue(tested.itMayBeMixedWithConditionalAnd());
	}
	//must return false
	public void testItMustBeLessPrecedentThanConditionalAnd(){
		Assert.assertFalse(tested.itMustBeLessPrecedentThanConditionalAnd());
	}
}
