package junit;

import pass.Palindrome;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This class verifies that the compiled code indeed satifies its
 * intended meaning.
 */
public class PalindromeTest extends TestCase {
	/** this is the tested object */
	private Palindrome tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new Palindrome();
	}
	
	
	public void testKayakIsAPalindrome(){
		Assert.assertEquals("kayak", tested.palindrome("kayak"));
	}
	
	public void testNoeIsNoPalindrome(){
		Assert.assertEquals("", tested.palindrome("noe"));
	}
	
	public void testNullIsAPalindrome(){
		Assert.assertNull(tested.palindrome(null));
	}
	
	public void testBlankStringIsAPalindrome(){
		Assert.assertEquals("   ", tested.palindrome("   "));
	}
	
	public void testItIsCaseInsensitive(){
		Assert.assertEquals("Kayak", tested.palindrome("Kayak"));
	}
}
