package junit;

import pass.Division;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * This unit test class runs the tests related to the pass.Division class that
 * was defined in order to validate that our modified version of j-- is able to
 * deal with our implementation of the euclidean division.
 */
public class DivisionTest extends TestCase {
	/** the tested object */
	private Division tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception{
		super.setUp();
		tested = new Division();
	}
	
	/** {@inheritDoc} */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	/**
	 * Our version of j-- must apply euclidean division between
	 * its terms.
	 */
	public void testDivide(){
		Assert.assertEquals(1, tested.divide(2, 2));
		Assert.assertEquals(3, tested.divide(9, 3));
		
		// it must leave a remainder (euclidean division)
		Assert.assertEquals(2, tested.divide(5, 2));
	}
	
	/**
	 * Our version of this operator must be able to cope with complex
	 * terms.
	 */
	public void itMustCopeWithComplexTerms(){
		Assert.assertEquals(2, tested.complexDivision());
	}
}
