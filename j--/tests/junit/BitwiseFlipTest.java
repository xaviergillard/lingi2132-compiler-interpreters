package junit;

import junit.framework.Assert;
import junit.framework.TestCase;
import pass.BitwiseFlip;

/**
 * This class verifies that the compiled code indeed satifies its
 * intended meaning.
 */
public class BitwiseFlipTest extends TestCase {
	/** this is the tested object */
	private BitwiseFlip tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new BitwiseFlip();
	}
	
	public void testTermCanBeLitteral(){
		Assert.assertEquals(-1, tested.termCanBeLitteral());
	}
	
	public void testTermCanBeNegatedInt(){
		Assert.assertEquals(0, tested.termCanBeNegatedInt());
	}
	
	public void testItHasTheSamePriorityAsUnaryMinus(){
		Assert.assertEquals(1, tested.itHasTheSamePriorityAsUnaryMinus());
	}
	
	public void testItHasPrecedenceOverMultiplicativeExpressions(){
		Assert.assertEquals(-3, tested.itHasPrecedenceOverMultiplicativeExpressions());
	}
	
	public void testItestItIsReversible(){
		Assert.assertEquals(3, tested.itIsReversible());
	}
}
