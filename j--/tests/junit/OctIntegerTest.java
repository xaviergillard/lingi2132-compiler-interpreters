package junit;

import junit.framework.Assert;
import junit.framework.TestCase;
import pass.OctInteger;

/**
 * This class verifies that the compiled code indeed satifies its
 * intended meaning.
 */
public class OctIntegerTest extends TestCase {
	/** this is the tested object */
	private OctInteger tested;
	
	/** {@inheritDoc} */
	protected void setUp() throws Exception {
		super.setUp();
		tested = new OctInteger();
	}
	
	// must return 1
	public void testItMayStartWithLeadingZeroes(){
		Assert.assertEquals(1, tested.itMayStartWithLeadingZeroes());
	}
	
	// must return 342391
	public void testItMayContainDigitsUpToSeven(){
		Assert.assertEquals(342391, tested.itMayContainDigitsUpToSeven());
	}

	// must return 2739128
	public void testZeroIsAValidDigit(){
		Assert.assertEquals(2739128, tested.zeroIsAValidDigit());
	}
}
