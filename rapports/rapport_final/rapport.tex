\documentclass{xav}

\usepackage{minted}

\institution {Université Catholique de Louvain    }
\faculty     {Ecole Polytechnique de Louvain      }
\course      {LINGI2132: Languages and Translators}
\title       {Jung DSL project                    }
\group       {Group 32                            }
\author      {BUEZ Olivier, GILLARD Xavier        }
\academicyear{Academic year: 2014-2015            }


\begin{document}
\maketitle

\section{Main features and specialties of the DSL}
\subsection{Non functional characteristics}
\subsubsection{The Build Tool}
The very first non-functional characteristic to note about the DSL we've created is that it was built using \emph{SBT}\footnote{http://www.scala-sbt.org} which is a build tool similar to Apache Maven. We used it to take advantage of the following features:
\begin{itemize}
	\item \textbf{Its dependency management capability} thanks to which it is made super easy for anyone to build the DSL library without having to deal with the usual dependency hell. This whole problem is sorted out automatically by the tool. On top of that, this feature also facilitates the DSL reuse since it only takes one line in the \textit{build.sbt} of the new project to add the dsl as a dependency and start using it.

	\item \textbf{Its automated tests run facility} thanks to which anyone can ensure of code quality at build time using the \verb|sbt test| command.
	
	\item \textbf{Its many integration with ad-hoc IDE plugins} which sets any potential maintainer free of choosing the IDE he prefers to work with.
\end{itemize}

\subsubsection{The tests and testing lib}
As a way to ensure of the quality of the code we were writing, we have written text specifications of what we wanted our DSL to be able to perform. Using a test driven approach, we have been writing tests validating each of the specifications items before we started to implement them. Not only has this provided a convenient way for us to determine when the implementation was satisfactory but it also has given us an appropriate framework to minimize the risks of regression and breakage of the code already written.

In order to write the specifications and corresponding tests in an easy and natural way, we have chosen to write the tests and specs using the \textit{ScalaTest}\footnote{http://www.scalatest.org} library. Beside that, we also have written numerous examples that serve two purposes: 
\begin{itemize}
	\item Try and validate the DSL syntax
	\item Verify manually the behavior that wasn't automatically testable (the visual aspect of things).
\end{itemize}

\subsection{Functional characteristics}
\subsubsection{Built on top Jung, not inside it}
The DSL was built \emph{on top of} Jung and not inside it. During the development, we really strived not to enforce anything on the end user and to put the effort on our library instead of the user. An example of this is that, in a first version of the project we had developed the language based on the assumption that an user could easily accommodate to work with a $Graph[Vertex[V], Edge[E]]$ instead of a $Graph[V,E]$. While this may seem not to be a limiting factor, it had two major drawback: 
\begin{itemize}
	\item The user had to be DSL-aware when manipulating and creating the graph (at least if not using the Graph object as a factory).
	\item The other interactions Jung proposes to the end user were much less convenient to work with. For instance, if the user wanted to use any of the advanced graph manipulation algorithms provided by Jung, it had to deal with our Vertex[V] and Edge[E] rather than directly with his V and E. Although this may seem perfectly inoffensive, it can turn out to be quite annoying to the user willing the V and E types to be generic too. E.g. because it is not possible to pattern match on the exact type of V and E because their parameter type has disappeared at runtime due to the \emph{erasure} mechanism.
\end{itemize}

\subsubsection{Valued and unvalued edges}
In the same order of idea as what we just explained, we have tried to provide the DSL-user with a convenient syntax to perform the manipulation he'd usually do while working with Jung. This implies that he would not only choose the value of the different vertices but also that of the edges. This is the very reason why we have introduced a specific syntax dedicated to handle this need, it looks as such: 
\verb|vertex1 -- edge_val --> vertex2| for a directed edge and \verb|vertex1 -- edge_val --- vertex2| for an undirected edge. Moreover, since in many case the user will just not care about the values of the different edges, we also provided a shorthand notation that bears no explicit value for the edges. It looks like \verb|vertex1 --> vertex2| for a directed edge and \verb|vertex1 --- vertex2| for an undirected edge.

\subsubsection{Fine grained, natural configuration}
Beside the great care we have paid to the graph creation, we have also designed our DSL in such a way that it is possible to write the configuration of the different elements of the graph in a very consistent, natural and compact way. For instance, one can simply specify how to render a vertex with the following sentence\footnote{The keyword in use are the same and have the same meaning for all items}: \verb|vertex drawIn Red fillWith Yellow withLabel "SomeVertex"|. Moreover, it is also possible for the user to customize the vertices using a 'field' notation which is maybe more usual to the average programmer.

\subsubsection{GUI dedicated sub-DSL}
Finally, we also wanted to abstract the end user from the burden of GUI programming when using our DSL. That's why we have decided to address this need by the means of a specific sub-DSL that permits the configuration and visualization of the graph with a sentence like the following one\footnote{Please note that this GUI-dedicated DSL offers a natural notation 200 x 300 to create a Dimension object.}:\\ \verb|Display(graph) withDimension (200 x 300) withLayout Static inEditMode|.

%\section{Examples}
%\todo{Si il reste de la place dans les 3 pages}
%
\section{Advanced DSL features that have been used.}
%During the development of our DSL, we have used plenty of advanced features in order to support the syntax we were building and, obviously, it wont be possible to detail all of their usage within the three pages of this paper. This is the reason why we will only cover and justify a small fraction of them in this report. The reader wishing for more in depth information should refer to the actual code documentation where everything is explained.
%
\subsection{Package-object}
A package object was used to group all the jungdsl features in one single scope making it possible for the calling application to load all the DSL features with one single import. (Concretely, \verb|import jungdsl._| is enough to get started using the DSL).

\subsection{Stacked traits}
Stacked traits have been used all over the place during the development of this project, in particular, they have been used in:
\begin{itemize}
	\item \textbf{the jungdsl package object} as a way to add all the features from the sub packages to the jungdsl scope so that the could be all imported at once.
	\item \textbf{the customisations and graph objects} as a way to perform some sort of 'aspect oriented programming'\footnote{This makes for highly cohesive design since all traits only focus on one single aspect of the behavior while the class only combines them to get its complete behavior}: we grouped under one trait all the behavior related to an aspect of the object (for instance the label-related capabilities, the ones related to the color configuration etc...) and the classes behavior were 'composed' with these building-blocks.
\end{itemize}

\subsection{Implicit}
In this project we have massively used the implicit constructs offered by the language (parameters, classes, conversions) as a way to alleviate the writing and remove some of the configuration burden from our DSL-user's shoulders to our library. In particular, we have used :
\begin{itemize}
	\item An \textbf{Implicit class} \textit{OngoingDimensionBuild} defined in the scope of the \textit{CustomisationUtils} trait, as a way to provide a natural writing for the dimensions.
	\item \textbf{Implicit fields} in the scope of the \textit{Factories} trait which lets us automatically and seamlessly provide the necessary edges and vertex factories that are needed when creating a graph and displaying it in edit mode.
	\item \textbf{Implicit conversions}. These are the daily butter and bread of our DSL which we used for instance in the definition of the GaphingUtils trait. Thanks to this, the user is no longer required to know the names of the classes that provide him the syntaxes he uses. In particular, he doesn't need to know anything about GraphOps, EdgeOps and VertexOps.
	\item \textbf{Implicit parameters} which are used in conjunction with the implicit fields to automatically feed the necessary factories. 
\end{itemize}

\subsection{Operator definition}
Operators definition is the construct that we have used in \textit{EdgeSource, ValuedEdgeSource and GraphOps} as a way to provide symbolic representations of the actions that can be performed in the language. In particular, this is how we have defined the \verb|+=| operator on the graphs and the \verb|-->| operator on the vertices.

\subsection{By-name, Closures and Currying}
By-name, closures and currying have also been used in the scope of this project. For instance by-name call have been used in the definition of the \textit{SideEffect} trait as a way to provide a new language construct embodying the application of some side effect.

Similarly, closures have been used in the scope of the \textit{RenderContextConfigurer} in order to determine how and what configuration to access. E.g., in the line \verb|transformer(graph.edges)(_.arrow.stroke)(defaultArrowStroke)|, the portion \verb|_.arrow.stroke| is a closure telling how to retrieve the arrow stroke configuration from an edge.

Finally, currying was massively used (together with scala reflection API) in order to provide a convenient way to seamlessly create Layout objects. Thanks to this mechanism, we were able to pass on an incomplete application of some layout constructor; making it easy to instantiate the appropriate layout where required.

\subsection{Objects and apply}
On top of all the mechanisms we already mentioned, our library also makes use of objects where the \textit{apply} method is defined. These objects can then serve as convenient factories for the end user. This was for instance the purpose of the \textit{Color} and \textit{GraphOps} objects.

\section{Conclusion: strengths and weaknesses}
So far, we think we made a pretty good job at providing an elegant and robust DSL implemented as an extra layer on top of Jung. This DSL simplifies the way the most common operations are performed while using that library. The greatest of its strengths being its ease of use, flexibility (all of the features from Jung remain accessible at all times) and compactness. Meanwhile, there are a few objections that could be raised regarding the implementation that was done; for instance: there might be a better approach than storing a map of $Graphs\rightarrow GraphOps$ in the GraphingUtils trait in order to be able to retrieve the instance that was properly configured. An other objection that could possibly be raised is that there should probably exist a more type-safe way of creating edges with no explicit value than what is currently done (using null and then, later on, replace that null value with the outcome of a call to factory.create()). But none of these observations constitute a major drawback either in term of usability or maintainability of the DSL library. 

Had we had more time, then we probably would have tackled the "hypergraph-edges" in order to give the user a more complete experience of what can possibly be done with Jung.

\clearpage\appendix\pagenumbering{alph}
\part*{Appendices}
\section{Summarizing example}
Since the previous report was bound to three pages only, we didn't have enough room to provide the reader with a convincing example of what can possibly be done with our DSL. Although one can easily find further examples in the source tree of our project\footnote{See \$PROJECT/src/test/scala/example for further examples}, we nevertheless have decided to reproduce here a self-documented code snippet showcasing the different features of the DSL we developed.

\begin{minted}[tabsize=2, fontsize=\footnotesize]{scala}
import jungdsl._
/** This example covers many of the features of the language */
object SummarizingExample extends App {
	// We can create graphs with some factory // or doit using the raw class
	val graph = Graph[String, Int]            // new SparseMultigraph[String, Int]
	
	// We can add verticess
	graph += "A"
	// We can customize the color (draw, fill), stroke and label of the edges while adding them
	graph += "B" drawIn Blue fillWith Blue withStroke Dotted withLabel "B"
	// We can customize them even after they are added
	graph("A").fill = Yellow                  // Using the field notation
	graph("B") withShape Square(20)           // or the keywords
	
	// We can add unvalued edge
	graph += "A" --- "B"
	graph += "C" --> "D"                      // it can add vertices to create an edge
	
	// We can add valued edges
	graph += "E" -- 3 --> "F"
	graph += "G" -- 4 --> "H"
	// we can have multiple edges for the same endpoints
	graph += "E" -- 5 --- "F"
	
	// We can customise the color (draw, fill), stroke and label of the edges
	graph += "Biip" -- 6 --> "Muut" drawIn Red fillArrowIn Red withStroke Dotted withLabel "GameBoy"
	
	// We can display the graph in edition mode for interactive edition (creation)
	// We can specify what layout and dimensions to use
	Display(graph) withDimension (200 x 300) withLayout Circle inEditMode
	// or we can show it in read only mode
	//Display(graph) inReadOnlyMode
}
\end{minted}

\end{document}