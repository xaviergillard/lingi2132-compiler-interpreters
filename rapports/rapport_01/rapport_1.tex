\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}

\usepackage[
	paper   = a4paper,
	tmargin = 1cm,
	headsep = .5cm,
	lmargin = 1.5cm,
	rmargin = 1.5cm,
	bmargin = 2cm
]{geometry}

\usepackage{minted}					% Ce package fournit la coloration syntaxique du code.
									%
									% http://tug.ctan.org/tex-archive/macros/latex/contrib/minted/minted.pdf
									%
									% Pour l'utiliser il faut: 
									% 	1) avoir installé Pygments (easy_install est un script python dc pas de prob)
									%		(sudo) easy_install Pygments
									%	2) il faut passer -shell-escape sur la ligne de commande.
									%		Si on utilise le makefile il n'y a aucun probleme.

%----------------------------------------------------------------------------------------
% COMMANDS
%----------------------------------------------------------------------------------------

%\catcode`\_=12
\newcommand{\underscore}{\catcode`\_=8}
\newcommand{\nounderscore}{\catcode`\_=12}
\nounderscore

\newcommand{\jminmin}{j-\hspace{2pt}-\hspace{2pt}}

\definecolor{gris}{rgb}{.95, .95, .95}

%----------------------------------------------------------------------------------------
% METADATA
%----------------------------------------------------------------------------------------

\author{Group 32: BUEZ Olivier, GILLARD Xavier}
\title{
	LINGI2132 - Assignment 1: \\
	Getting familiar with the \jminmin compiler.
}

\begin{document}

\maketitle

% Foreach program: 
% 		CHOICES
%		TESTING => Quels tests ?
%				=> Pq ce test case-la ?
%				=> Quel résultat
%		CHANGES TO GRAMMAR
\paragraph{Division}
The 'division' part of the assignment basically involved nothing but reproducing the changes that are described in the textbook. As a consequence of that, there really was no choice to be made when implementing this portion of the assignment. Therefore, since we knew that this piece of code would be compliant with the expectations\footnote{Credits for it should in the end go to Campbell, Iyer and Akbal-Delibas}, we have chosen to stick to the proposed implementation and tests. Only did we add one extra check to test the precedence of division over addition.

%Concretely, we started writing the tests (classes Division in pass and fail package + DivisionTest in junit package). Then we added some $DIV("/")$  token kind value in the $TokenKind$ enumeration class and implemented the AST-node representing the division as a class $JDivideOp$ extending from $JBinaryOp$ and appended it to the JBinaryOp.java file. Once this was done, we updated the $multiplicativeExpression()$ method from the $Parser$ class so that it would correctly parse an expression involving a division operation.

The grammatical changes we made were the following: we added a token 'DIV' in the lexical grammar and updated the grammar as such:
\begin{center}
\begin{minted}[bgcolor=gris]{c++}
 multiplicativeExpression ::= unaryExpression {(STAR | DIV) unaryExpression}
\end{minted}
\end{center}

\paragraph{Left shift}
The part concerning the left shift was slightly more advanced than the division because we had to decide whether to implement this operation as a \emph{multiplication operation} (which makes sense since $x<<n = x*2^n$) or to introduce a new level of precedence in the grammar. In the end we opted for the latter option in order to let our implementation be compliant with the JLS\footnote{Java Language Specification, paragraph 15.19, http://docs.oracle.com/javase/specs/jls/se5.0/html/expressions.html }. During the definition of the tests, we have tried to be much more thorough and systematic than we had been implementing the division operator: in the class \emph{fail.LeftShift} we tested some instance of all the syntactic and semantic errors we could think of (namely, we verified that both operands were present, that infix notation was used and that this new operation could only operate on integers), in the \emph{pass.LeftShift} and \emph{junit.LeftShiftTest} we tested some instance of all the valid use case we could think of (Namely that members can be litterals that are potentially  negated, that the terms could be mutiplicative/additive expressions, that only the five low order bits are used to perform the shift [interval bound by 0 - 31] and that left shifts can be chained together to form a bigger expression). 

The left shift operator was the only one that caused us some problems. This was in actually due to the fact that in a first version of our work, we implemented the \emph{jminusminus.Parser.shiftExpression()} method with a right recursion instead of a loop\footnote{This was a deviation from the JLS that states that shift operators are left associative. This deviation vanished when we decided to opt for a loop based implementation.}.

The grammatical changes we made were the following:
\begin{center}
\begin{minted}[bgcolor=gris]{c++}
// This has been modified because of the new 'priority level' we introduced
relationalExpression ::= shiftExpression [(GT | LE) shiftExpression | INSTANCEOF referenceType]
// This is our new rule
shiftExpression      ::= additiveExpression {LSHIFT additiveExpression}
\end{minted}
\end{center}

\paragraph{Unary bitwise complement operator} 
In order to implement this operator, we introduced a new token named \emph{FLIP}. Just like we did for the two previous exercises, we have defined a new class implementing the AST node corresponding to this syntactical construct. The difference being that we defined this class in the \emph{jminusminus/JUnary\-Expression.java} file instead of \emph{jminusminus/JBinaryExpression.java} for the previous two. Since this operator is very similar (and has the same precedence as) the unary minus, we decided to update the \emph{jminusminus.Parser.unary\-Expression()} method in order to give it the ability to recognize a bitwise complement expression.

The grammatical changes we made were thus the following:
\begin{center}
\begin{minted}[bgcolor=gris]{c++}
 unaryExpression ::= INC unaryExpression 
                   | MINUS unaryExpression
                   | FLIP unaryExpression   // This is new !
                   | simpleUnaryExpression
\end{minted}
\end{center}

Just like we did for the left shift operator, we have implemented quite a few test cases to validate our implementation against its expected behavior: compilation must fail whenever one of the operand is misplaced, the operand is missing, the operator is used as a binary operator or it is used with an other type than integer. Moreover, we checked that the compiler properly understood an expression with an operand being a potentially negated litteral, that $\sim$ has the same priority as the unary minus, that it has precedence over multiplicative and additive expressions. Finally, we also checked that it is reversible ($\sim\sim n = n$).

\paragraph{Palindrome}
In order to implement the Palindrome program, we have chosen to define the following functions: 
\begin{itemize}
\item \textbf{static void main(String[]args)} which is our program entry point
\item \textbf{String palindrome(String    s)} which is the one imposed by the assignment specification.
\item \textbf{boolean isPalindrome(String s)} that returns true only when the given input string is a palindrome.
\item \textbf{static void die(String    msg)} that outputs some given message to standard error and terminates the program.
\end{itemize}

The implementation of the isPalindrome test is fairly simple and is based on the usage of the \emph{java.lang.StringBuilder} class. This taught us that, as oposed to javac, \jminmin doesn't perform an automatic import of the classes in the java.lang package; neither does it performs the autoboxing we're used to work with, since it requires an explicit cast to be done in order to use the \emph{equals(Object o)} method.

Regarding the testing of this program, we focused on testing the method \emph{String palindrome(String)} that was imposed by the INGInious testbench. In order to prepare for it, we implemented some junit tests validating some words that had to be accepted ("Kayak", "kayak", empty string, null) or rejected ("Noe") keeping in mind that the test should be case insensitive.

Please note that we also made an alternate working implementation of the isPalindrome method using only String api and built-in \jminmin constructs but we have decided not to submit it since we have considered this code to be less elegant altough it is more performant\footnote{It was nevertheless committed and pushed to the bitbucket repository we used, so the resource can be accessed here: http://tinyurl.com/qdyygch}.

\paragraph{Final word about the result of the tests}
Since we have used a TDD\footnote{Test Driven Development} approach (tests were written \emph{\textbf{before}} implementation code was), we have used our tests (fail, pass and junit) as a  behavioral contract that our implementation had to satify. As a consequence of that, all the tests we mentionned were sucessfully completed. This means that either an appropriate error message has been logged on stderr (fail tests), or all assertions we made about the implementation were verified (pass + junit tests).

\end{document}