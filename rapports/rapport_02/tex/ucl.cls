%
% Class that I use for my usual reports and so on.
%
% Author : X. GILLARD 
% Date   : 22/02/2015
% Version: 1.0
%
% ----------------------------------------------------------------------------------
% HOW TO INSTALL THE emerald PACKAGE
% __Installation on OS X__
%	sudo mv ~/Downloads/emerald $(kpsewhich --var-value TEXMFLOCAL)
%	sudo texhash
%   sudo updmap-sys --enable Map=emerald.map
%
% ----------------------------------------------------------------------------------
% HOW TO INSTALL THE Pygments python PACKAGE
%
% Installation is done with;
%   sudo easy_install Pygments 
%
% Sadly, compilation REQUIRED YOU to provide the -shell-escape flag
%
%----------------------------------------------------------------------------------------

\ProvidesClass{ucl}

\LoadClass[a4paper, 12pt]{article}

\RequirePackage[utf8]{inputenc}			% use any kind of character in the body
\RequirePackage[T1]{fontenc}			% let it be able to produce any character
\RequirePackage{microtype}				% better looking text distribution
%\RequirePackage{emerald}				% some fonts (augie) (..fun but requires setup)
%\RequirePackage[default]{comfortaa}		% some other fonts (default font is comfortaa)
\usepackage{lmodern}

\RequirePackage{multicol}
\RequirePackage{pdflscape}
\RequirePackage{float}
\RequirePackage{placeins}           	% Provides \FlooatBarrier to force a float positioning 

\RequirePackage{graphicx}				% images

\RequirePackage{tikz}					% coloured boxes etc..
%\RequirePackage[tikz]{bclogo}			% 
% \RequirePackage{minted}				% syntax coloration (very handy but requires some setup)

\RequirePackage{ifthen}					% conditional expressions

\RequirePackage{titlesec}				% customize the sectionning commands
\RequirePackage{titletoc}				% customize tableof contents

\RequirePackage{tabto}					% Being able to 'jump' to some given location
\RequirePackage{enumitem}
\RequirePackage{calc}

\newcommand{\@thealternatefont}{\normalfont}
\newcommand{\alternatefont}[1]{
	\renewcommand{\@thealternatefont}{#1}
}

%----------------------------------------------------------------------------------------
% Colors
%----------------------------------------------------------------------------------------
\RequirePackage{xcolor}
\definecolor{default}  {rgb}{0  , 0.56,   1}
\definecolor{black}    {rgb}{0  , 0   ,   0}
\definecolor{red}      {rgb}{255, 0   ,   0}
\definecolor{blue}     {rgb}{0  , 0   , 255}
\definecolor{blue-green}{rgb}{0.0, 0.87, 0.87}
\definecolor{lightgray}{rgb}{0.83, 0.83, 0.83}
\definecolor{airforceblue}{rgb}{0.36, 0.54, 0.66}

%----------------------------------------------------------------------------------------
% Page setup
%----------------------------------------------------------------------------------------
\RequirePackage{fancyhdr}
\RequirePackage[						% page setup
	a4paper, 
	headsep=10pt,
	top=1cm,
	bottom=2cm,
	inner=1.5cm,
	outer=1.5cm
]{geometry}		

%
% Page number to the right
%		
\renewcommand{\headrulewidth}{0pt}

\fancypagestyle{plain}{
	\fancyhf{}
	\fancyfoot[R]{
		\textcolor{lightgray}{\@title} / \thepage
	}
}
\pagestyle{plain}

%----------------------------------------------------------------------------------------
% Because I like to use underscore as a normal character
%----------------------------------------------------------------------------------------

\newcommand{\underscore}{\catcode`\_=8}
\newcommand{\nounderscore}{\catcode`\_=12}
\nounderscore

%----------------------------------------------------------------------------------------
% Utilities
%----------------------------------------------------------------------------------------
\newenvironment{bottom}
	{\vspace*{\fill}}		% fills the page with vertical blank above your text
	{\clearpage}			% inserts a page break but flushes all floating stuffs

\newcommand{\bottompar}[1]{\begin{bottom}#1\end{bottom}}

%
% A line that spans horizontally through the whole page
%
\newcommand{\HLINE}{
	\noindent\hspace{-5cm}\makebox[1.5\paperwidth]{ \rule{1.5\paperwidth}{0.4pt} } 
}

%------------------------
% Boites de couleur
%------------------------

% 
% Box 
% #1 = colour (optional)
% #2 = text
%
\newcommand{\boite}[2]{
	\vspace*{1em}\par\noindent%
	\begin{tikzpicture}
		\draw (0, 0) node [
			rounded corners=5pt, 
			fill=#1, 
			text=black,
			fill opacity=.1, 
			text opacity=1
		] {
			\begin{minipage}{\textwidth}
			#2
			\end{minipage}
		};
	\end{tikzpicture}
}
\newcommand{\todo}[1]{\boite{red}{\textbf{TODO}\\#1}}
\newcommand{\question}[1]{\boite{red}{\textbf{Question}\\#1}}
\newcommand{\important}[1]{\boite{red}{#1}}
\newcommand{\note}[1]{\boite{yellow}{#1}}

%----------------------------------------------------------------------------------------
% Title page
%----------------------------------------------------------------------------------------
%
% What is the institution ?
%
\newcommand{\@theinstitution}{}
\newcommand{\institution}[1]{\renewcommand{\@theinstitution}{#1}}

%
% What faculty ?
%
\newcommand{\@thefaculty}{}
\newcommand{\faculty}[1]{\renewcommand{\@thefaculty}{#1}}

%
% What course/class
%
\newcommand{\@thecourse}{}
\newcommand{\course}[1]{\renewcommand{\@thecourse}{#1}}

%
% What subgroup
%
\newcommand{\@thegroup}{}
\newcommand{\group}[1]{\renewcommand{\@thegroup}{#1}}

%
% What academic year ?
%
\newcommand{\@theacademicyear}{}
\newcommand{\academicyear}[1]{\renewcommand{\@theacademicyear}{#1}}

%
% Defines a title page
%
\renewcommand{\titlepage}{
	\begingroup
		\thispagestyle{empty}
		\setcounter{page}{0}
				
		\centering
		{\textsc \@theinstitution} \\
		{\textbf \@thefaculty}     \\
		\HLINE

		\vspace*{7cm}
		{\Large\textit\@thecourse} \par\vspace*{.5cm}

		\@thealternatefont
		{\LARGE\textbf\@title}	   \par\vspace*{2cm}
		\normalfont

		{\textbf{\@thegroup}}      \par\vspace*{.5cm}
		{\@author}
		

		\begin{bottom}
			\HLINE \par\vspace*{.5cm}
			\@theacademicyear
		\end{bottom}
	\endgroup
}
\def\maketitle{\titlepage}

%----------------------------------------------------------------------------------------
% Table of contents
%----------------------------------------------------------------------------------------
%
% Redefine the toc title
%
\newcommand{\@toctitle}[1]{
		\renewcommand{\contentsname}{#1}
}

\newcommand{\toc}[1]{
	\@toctitle{#1}
	\tableofcontents
	\vfill
}